 'use strict';

 var language = require('./Languages');

 var ADMIN_ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU4YTMzODk4NmE4NmVlNDg4MjA1NTRmNyIsImRldmljZVRva2VuIjoiYXMiLCJkZXZpY2VUeXBlIjoiSU9TIiwiZGV2aWNlSWQiOiJhc2RhZCIsInR5cGUiOiJVU0VSIiwidGltZSI6MTQ4NzE3MDY4NDc1MCwiaWF0IjoxNDg3MTcwNjg0fQ.8XBPjzTYm6pWoDpG15Lowo7bSjbT4eLnzqokvqrJfiM";

 var env = {
     DEV: 'DEV',
     LIVE: 'LIVE',
     LOCAL: 'LOCAL'
 }
 var objectKeys = function(obj) {
     return Object.keys(obj);
 }

 var objectValues = function(obj) {
     var ar = [];
     Object.values(obj).forEach(function(k) {
         ar.push(obj[k]);
     });
     return ar;
 }

 var objectKeys = function(obj) {
     return Object.keys(obj);
 }

 var objectValues = function(obj) {

     let ar = [];

     Object.keys(obj).forEach(function(k) {
         ar.push(obj[k]);
     })
     return ar;
 }


 var SERVER = {
     env: env,
     APP_NAME: 'Kid-Circle',
     PORTS: {
         HAPI: 7007
     },
     TOKEN_EXPIRATION_IN_MINUTES: 600,
     JWT_SECRET_KEY: 'sUPerSeCuREKeY$$KidCircle$$&^$^&$^%C$^%34Ends',
     GOOGLE_API_KEY: 'AIzaSyBO2bhirASSGwMDzaO64hisl7CMDX1_Whg',
     COUNTRY_CODE: '+1',
     THUMB_WIDTH: 100,
     THUMB_HEIGHT: 100,
     DOMAIN_NAME: 'http://www.kidcircle.com',
     SUPPORT_EMAIL: '',
     DEBUGGING: false,
     ENV: env.DEV


 };


 var POST_IMAGE_SIZE = {

     width: 630,
 }

 var PAGE_LIMIT = {
     NOTIFICATION: 5,
     USER_FILTER: 10,
     BOOKING_HISTORY: 20,
     DEFAULT_LIMIT: 50
 }

 var USER_FILTER_DISTANCE = {
     MIN: 0,
     MAX: 25
 }


 var NOTIFICATIONS = {
     TYPE: {
         BOOKING: "booking"
     },
     PUSH: {
         NEW_BOOKING: {
             FLAG: 100,
             MESSAGE: "New Booking Received."
         },
         BOOKING_RESCHEDULE: {
             FLAG: 101,
             MESSAGE: "Booking has been rescheduled by the patient."
         },
         BOOKING_REMINDER: {
             FLAG: 102,
             MESSAGE: "You have a booking in a while. Check the details here!"
         }
     }

 }

 var WEB_VIEW_TITLE = {

 }

 var DATABASE = {
         USER_ROLES: {
         ADMIN: 'ADMIN',
         USER: 'USER',
         VENDOR: 'VENDOR'
     },
     maritalStatus: {
         Single: 'single',
         Married: 'married',
         Widow: 'widow',
         Divorced: 'divorced',
         Separated: 'separated'
     },
     gender: {
         Male: 'Male',
         Female: 'Female',
         Transgender: 'Transgender'
     },

     sectionCategory: {
        SYNONAMES_ANTONYMS : "Synonyms And Antonyms",
        ANALOGIES : "Analogies",
        MEANING_MAKING_CROSSWORD : "Meaning Making Crossword",
        MEANING_MAKING : "Meaning Making",
        JUMBLED_SENTENCES : "Jumbled Sentences",
        GRAMMATICAL_ERRORS : "Grammatical Errors",
        NUMBER_RIDDLES : "Number Riddles",
        NUMBERICAL_THINKING : "Numerical Thinking",
        LINGUAL_CRITICAL_THINKING : "Lingual Critical Thinking",
        JUMBLED_WORDS : "Jumbled Words",
        DECODE_THE_RIDDLES : "Decode The Riddles",
        NUMBER_PATTERN : "Number Pattern",
        NUMER_SERIES_COMPLETION : "Number Series Completion",
        DATA_COMPLETION : "Data Completion",
        PERSONALITY : "Personality"
     },
     meaningMaking :{
        ACCROSS : "ACCROSS",
        DOWN: "DOWN"
     },
     questionTypes: {
         MCQ : 'Multiple Choice Questions',
         PERSONALITY : "Personality",
         // CROSSWORDS : "Crosswords",
         // MEANING_MAKING : "Meaning Making",
         // JUMBLED_SENTENCES : "Jumbled Sentences",
         // GRAMMATICAL_ERRORS : "Grammatical Errors",
         // IMAGE_JUMBLED_WORDS: "Image Jumbled Words",
         // IMAGE_RIDDLES : "Image Riddles",
         DIRECT_ANSWER : "Direct Answer",
         MULTIPLE_DIRECT_ANSWER: "Multiple Direct Answer"
     } ,
     deviceType: {
         IOS: 'ios',
         Android: 'android',
         Website: 'web'
     },
     userType: {
         User: 'user',
         Admin: 'admin'
     },
     userRole: {
         Parent: 'parent',
         Vendor: 'vendor'
     },
     bookingStatus: {
         Approved: 'approved',
         Cancelled: 'cancelled',
         Expired: 'expired',
         Pending: 'pending',
         Completed: 'completed',
         null: null
     },
     permissions: {
         None: "none",
         Read: "read",
         Add: "add",
         Edit: "edit",
         Delete: "delete"
     },
     profilePicPrefix: {
         Original: 'original_',
         Thumb: 'thumb_'
     },
     logoPrefix: {
         Original: 'logo_',
         Thumb: 'logoThumb_'
     },
     documentPrefix: 'document_',
     registeredFrom: {
         webPanel: 'web-panel',
         adminPanel: 'admin-panel',
         app: 'app'
     },
     couponType: {
         Cashback: 'cashback',
         Discount: 'discount',
     },
     discoutnType: {
         Percent: 'percent',
         Unit: 'unit',
     },
     bookingFrom: {
         Ios: 'ios',
         Adroid: 'android',
         Website: 'website',
         AdminPanel: 'admin-panel'
     },
     language: {
         EN: 'EN',
         ES_MX: 'ES_MX',
         HIN: 'HIN'
     },
     socketConstants: {
         NewNotification: "new-notification",
         AdminNotification: "admin-notification",
         NewMessage: "new-message",
         NewFileMessage: "new-file-msg"
     },
     mediaTypes: {
         File: "file",
         Image: "image",
         Video: "video",
         Audio: "audio"
     },
     contactQueryStatus: {
         Pending: "Pending",
         UnderDiscussion: "Under Discussion",
         Resolved: "Resolved"
     },
     subscriptionStatus: {
         Active: "Active",
         Unsubscribed: "Unsubscribed"
     },

 }

 var USER_CONSTANTS = {

     maritalStatus: {
         server: objectKeys(DATABASE.maritalStatus),
         show: objectValues(DATABASE.maritalStatus)
     },
     gender: {
         server: objectKeys(DATABASE.gender),
         show: objectValues(DATABASE.gender)
     },
     deviceType: {
         server: objectKeys(DATABASE.deviceType),
         show: objectValues(DATABASE.deviceType)
     },
     bookingStatus: {
         server: objectKeys(DATABASE.bookingStatus),
         show: objectValues(DATABASE.bookingStatus)
     },
     userRole: {
         server: objectKeys(DATABASE.userRole),
         show: objectValues(DATABASE.userRole)
     },
     userType: {
         server: objectKeys(DATABASE.userType),
         show: objectValues(DATABASE.userType)
     },
     mediaTypes: {
         server: objectKeys(DATABASE.mediaTypes),
         show: objectValues(DATABASE.mediaTypes)
     },
     questionTypes: {
         server: objectKeys(DATABASE.questionTypes),
         show: objectValues(DATABASE.questionTypes)
     },
     sectionCategory: {
        server: objectKeys(DATABASE.sectionCategory),
        show: objectValues(DATABASE.sectionCategory)
     },
     meaningMaking : {
        server: objectKeys(DATABASE.meaningMaking),
        show: objectValues(DATABASE.meaningMaking)
     }
 };

 var STATUS_MSG = {

     ERROR: {

         customError: function(message) {

             return {
                 statusCode: 400,
                 success: false,
                 type: 'CUSTOM_ERROR',
                 customMessage: message

             }
         },

         NOT_AUTHENTICATED_FACEBOOK_USER: {


         },
           NOT_AUTHORIZED_TO_VIEW_IMAGE: {
             statusCode: 400,
             success: false,
             type: 'NOT_AUTHORIZED_TO_VIEW_IMAGE',
             customMessage: 'You are not authorized to view this image.'

         },

         NOT_AUTHORIZED_TO_PERFORM_THIS_REQUEST: {
             statusCode: 400,
             success: false,
             type: 'NOT_AUTHORIZED_TO_PERFORM_THIS_REQUEST',
             customMessage: 'You are not authorized to perfom this request.'

         },
         REVEIW_OF_UNDEFINED: {
             statusCode: 400,
             success: false,
             type: 'REVEIW_OF_UNDEFINED',
             customMessage: 'Please send the review entity!'
         },
         NOT_ASSOCIATED_WITH_US: {
             statusCode: 400,
             success: false,
             type: 'NOT_ASSOCIATED_WITH_US',
             customMessage: 'User is not associated with ' + SERVER.APP_NAME + ' anymore. please contact support!'
         },
         INVALID_ID_PROVIDED: {
             statusCode: 400,
             success: false,
             type: 'INVALID_ID_PROVIDED',
             customMessage: 'Unable to delete! Invalid Id provided'
         },
         INVALID_CONSTANT_TYPE: {
             statusCode: 400,
             success: false,
             type: 'INVALID_CONSTANT_TYPE',
             customMessage: 'Invalid Constant Type'
         },
         INTEREST_NAME_EXIST: {
             statusCode: 400,
             success: false,
             type: 'INTEREST_NAME_EXIST',
             customMessage: 'Interest name already exist!'
         },
         CUSTOM_ERROR: {
             statusCode: 400,
             success: false,
             type: 'CUSTOM_ERROR',
             customMessage: 'CUSTOM_ERROR'
         },
         INVALID_PINCODE_ID: {
             statusCode: 400,
             success: false,
             type: 'INVALID_PINCODE_ID',
             customMessage: 'Invalid pincode id'
         },
         COUPON_ALREADY_APPLIED: {
             statusCode: 400,
             success: false,
             type: 'COUPON_ALREADY_APPLIED',
             customMessage: 'Sorry, But this coupon is already applied by you.'
         },
         REQUEST_NOT_FOUND: {
             statusCode: 400,
             success: false,
             type: 'REQUEST_NOT_FOUND',
             customMessage: 'Invalid request there is no entity related to it'
         },
         INVALID_DELETION: {
             statusCode: 400,
             success: false,
             type: 'INVALID_DELETION',
             customMessage: 'Invalid deletion there is an entity related to it'
         }, INVALID_POSTING_KEY: {
             statusCode: 400,
             success: false,
             type: 'INVALID_POSTING_KEY',
             customMessage: 'Username or posting key is not correct!'
         },
         ACCOUNT_BLOCKED: {
             statusCode: 400,
             success: false,
             type: 'ACCOUNT_BLOCKED',
             customMessage: 'Your account has been blocked by Admin.'
         },
         INVALID_USER_PASS: {
             statusCode: 400,
             success: false,
             type: 'INVALID_USER_PASS',
             customMessage: 'Invalid username or password'
         },
         TOKEN_ALREADY_EXPIRED: {
             statusCode: 401,
             success: false,
             customMessage: 'Token Already Expired',
             type: 'TOKEN_ALREADY_EXPIRED'
         },
         DB_ERROR: {
             statusCode: 400,
             success: false,
             customMessage: "Sorry , It's not you .\n It's Us.",
             type: 'DB_ERROR'
         },
         INVALID_ID: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Id Provided : ',
             type: 'INVALID_ID'
         },
         APP_ERROR: {
             statusCode: 400,
             success: false,
             customMessage: 'Application Error',
             type: 'APP_ERROR'
         },
         ADDRESS_NOT_FOUND: {
             statusCode: 400,
             success: false,
             customMessage: 'Address not found',
             type: 'ADDRESS_NOT_FOUND'
         },
         IMP_ERROR: {
             statusCode: 500,
             success: false,
             customMessage: 'Implementation Error',
             type: 'IMP_ERROR'
         },
         APP_VERSION_ERROR: {
             statusCode: 400,
             success: false,
             customMessage: 'One of the latest version or updated version value must be present',
             type: 'APP_VERSION_ERROR'
         },
         INVALID_TOKEN: {
             statusCode: 401,
             customMessage: 'Invalid token provided',
             type: 'INVALID_TOKEN'
         },
         INVALID_CODE: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Verification Code',
             type: 'INVALID_CODE'
         },
         DEFAULT: {
             statusCode: 400,
             success: false,
             customMessage: 'Error',
             type: 'DEFAULT'
         },
         PHONE_NO_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'Phone No Already Exist',
             type: 'PHONE_NO_EXIST'
         },
         EMAIL_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'Email Already Exist',
             type: 'EMAIL_EXIST'
         },
         USERNAME_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'This username is already taken.',
             type: 'USERNAME_EXIST'
         },
         NO_REQ_FOUND: {
             statusCode: 400,
             success: false,
             customMessage: 'No such verfication request found.',
             type: 'NO_REQ_FOUND'
         },
         REQ_ALREADY_UPDATED: {
             statusCode: 400,
             success: false,
             customMessage: 'You have already updated this verification request.',
             type: 'REQ_ALREADY_UPDATED'
         },
         DUPLICATE: {
             statusCode: 400,
             success: false,
             customMessage: 'Duplicate Entry',
             type: 'DUPLICATE'
         },
         DUPLICATE_ADDRESS: {
             statusCode: 400,
             success: false,
             customMessage: 'Address Already Exist',
             type: 'DUPLICATE_ADDRESS'
         },
         UNIQUE_CODE_LIMIT_REACHED: {
             statusCode: 400,
             success: false,
             customMessage: 'Cannot Generate Unique Code, All combinations are used',
             type: 'UNIQUE_CODE_LIMIT_REACHED'
         },
         INVALID_REFERRAL_CODE: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Referral Code',
             type: 'INVALID_REFERRAL_CODE'
         },
         FACEBOOK_ID_PASSWORD_ERROR: {
             statusCode: 400,
             success: false,
             customMessage: 'Only one field should be filled at a time, either facebookId or password',
             type: 'FACEBOOK_ID_PASSWORD_ERROR'
         },
         INVALID_EMAIL: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Email Address',
             type: 'INVALID_EMAIL'
         },
         PASSWORD_REQUIRED: {
             statusCode: 400,
             success: false,
             customMessage: 'Password is required',
             type: 'PASSWORD_REQUIRED'
         },
         INVALID_COUNTRY_CODE: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Country Code, Should be in the format +52',
             type: 'INVALID_COUNTRY_CODE'
         },
         INVALID_PHONE_NO_FORMAT: {
             statusCode: 400,
             success: false,
             customMessage: 'Phone no. cannot start with 0',
             type: 'INVALID_PHONE_NO_FORMAT'
         },
         COUNTRY_CODE_MISSING: {
             statusCode: 400,
             success: false,
             customMessage: 'You forgot to enter the country code',
             type: 'COUNTRY_CODE_MISSING'
         },
         INVALID_PHONE_NO: {
             statusCode: 400,
             success: false,
             customMessage: 'Phone No. & Country Code does not match to which the OTP was sent',
             type: 'INVALID_PHONE_NO'
         },
         PHONE_NO_MISSING: {
             statusCode: 400,
             success: false,
             customMessage: 'You forgot to enter the phone no.',
             type: 'PHONE_NO_MISSING'
         },
         NOTHING_TO_UPDATE: {
             statusCode: 400,
             success: false,
             customMessage: 'Nothing to update',
             type: 'NOTHING_TO_UPDATE'
         },
         NOT_FOUND: {
             statusCode: 400,
             success: false,
             customMessage: 'User Not Found',
             type: 'NOT_FOUND'
         },
         INVALID_RESET_PASSWORD_TOKEN: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Reset Password Token',
             type: 'INVALID_RESET_PASSWORD_TOKEN'
         },
         INCORRECT_PASSWORD: {
             statusCode: 400,
             customMessage: 'Incorrect Password.',
             type: 'INCORRECT_PASSWORD'
         },
         EMPTY_VALUE: {
             statusCode: 400,
             success: false,
             customMessage: 'Empty String Not Allowed',
             type: 'EMPTY_VALUE'
         },
         PHONE_NOT_MATCH: {
             statusCode: 400,
             success: false,
             customMessage: "Phone No. Doesn't Match",
             type: 'PHONE_NOT_MATCH'
         },
         SAME_PASSWORD: {
             statusCode: 400,
             success: false,
             customMessage: 'Old password and new password are same',
             type: 'SAME_PASSWORD'
         },
         ACTIVE_PREVIOUS_SESSIONS: {
             statusCode: 400,
             success: false,
             customMessage: 'You already have previous active sessions, confirm for flush',
             type: 'ACTIVE_PREVIOUS_SESSIONS'
         },
         INVALID_EST_ID: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid EstablishmentId',
             type: 'INVALID_EST_ID'
         },
         EMAIL_ALREADY_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'Email Address Already Exists',
             type: 'EMAIL_ALREADY_EXIST'
         },
         ERROR_PROFILE_PIC_UPLOAD: {
             statusCode: 400,
             success: false,
             customMessage: 'Error occured in Profile Pic upload.',
             type: 'ERROR_PROFILE_PIC_UPLOAD'
         },
         PHONE_ALREADY_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'Phone No. Already Exists',
             type: 'PHONE_ALREADY_EXIST'
         },
         INVALID_JSON: {
             statusCode: 400,
             success: false,
             customMessage: 'Invalid Json Format.',
             type: 'INVALID_JSON'
         },
         EMAIL_NOT_FOUND: {
             statusCode: 400,
             success: false,
             customMessage: 'You are not registered with us.',
             type: 'EMAIL_NOT_FOUND'
         },
         FACEBOOK_ID_NOT_FOUND: {
             statusCode: 400,
             success: false,
             customMessage: 'Facebook Id Not Found',
             type: 'FACEBOOK_ID_NOT_FOUND'
         },
         PHONE_NOT_FOUND: {
             statusCode: 400,
             success: false,
             customMessage: 'Phone No. Not Found',
             type: 'PHONE_NOT_FOUND'
         },
         INCORRECT_OLD_PASS: {
             statusCode: 400,
             success: false,
             customMessage: 'Incorrect Old Password',
             type: 'INCORRECT_OLD_PASS'
         },
         UNAUTHORIZED: {
             statusCode: 401,
             success: false,
             customMessage: 'You are not authorized to perform this action',
             type: 'UNAUTHORIZED'
         },
         UNAUTHORIZED_ACCESS: {
             statusCode: 400,
             success: false,
             customMessage: 'You are not authorized to perform this action',
             type: 'UNAUTHORIZED_ACCESS'
         },
         VENDOR_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'A vendor is already exist with same email or phone number.',
             type: 'VENDOR_EXIST'
         },
         VENDOR_NOT_EXIST: {
             statusCode: 400,
             success: false,
             customMessage: 'You are not registered as a Vendor.',
             type: 'VENDOR_NOT_EXIST'
         },
         INVALID_ADDRESS: {
             statusCode: 400,
             success: false,
             type: 'INVALID_ADDRESS',
             customMessage: 'Pick-up or Drop-off address is not valid.'

         },
         ALREADY_PLACED: {
             statusCode: 400,
             success: false,
             type: 'ALREADY_PLACED',
             customMessage: 'This booking is already confirmed.'

         },
         INVALID_COUPON_APPLIED: {
             statusCode: 400,
             success: false,
             type: 'INVALID_COUPON_APPLIED',
             customMessage: 'Invalid coupon code.'
         },
         NO_OF_USERS_EXCEED: {
             statusCode: 400,
             success: false,
             type: 'NO_OF_USERS_EXCEED',
             customMessage: 'You missed the deal. This coupon is now invalid.'
         },
         COUPON_EXPIRED: {
             statusCode: 400,
             success: false,
             type: 'COUPON_EXPIRED',
             customMessage: 'This coupon has been expired.'
         },
         CANT_DELETE: {
             statusCode: 400,
             success: false,
             type: 'CANT_DELETE',
             customMessage: 'This booking cannot be deleted now.'
         },
         CANT_EDIT: {
             statusCode: 400,
             success: false,
             type: 'CANT_EDIT',
             customMessage: 'This booking cannot be edited now.'
         },
         NO_SERVICE: {
             statusCode: 400,
             success: false,
             type: 'NO_SERVICE',
             customMessage: 'We are not providing services in your area right now.'
         },
         PAYMENT_DETAILS_REQUIRED: {
             statusCode: 400,
             success: false,
             type: 'PAYMENT_DETAILS_REQUIRED',
             customMessage: 'Payment details are required or you can also pay with cash.'
         },
         SERVER_ERROR: {
             statusCode: 400,
             success: false,
             type: 'SERVER_ERROR',
             customMessage: 'There is some error at server side. Will fix this soon.'
         },
         NO_FILE_FOUND: {
             statusCode: 400,
             success: false,
             type: 'NO_FILE_FOUND',
             customMessage: "File not found.."
         },
         RECORD_NOT_FOUND: {
             statusCode: 400,
             success: false,
             type: 'RECORD_NOT_FOUND',
             customMessage: "Record not found."
         },
         CANT_FIND: {
             statusCode: 400,
             success: false,
             type: 'CANT_FIND',
             customMessage: "User not found."
         }

     },
     SUCCESS: {

         PLEASE_ENTER_OTP: {
             statusCode: 201,
             success: true,
             customMessage: 'Please verify your phone number. Your otp has been sent to your registered number.',
             type: 'PLEASE_ENTER_OTP'
         },

         PHONE_NOT_FOUND: {
             statusCode: 210,
             success: true,
             customMessage: 'Please provide your Phone Number.',
             type: 'PHONE_NOT_FOUND'
         },

         PHONE_NOT_VERIFIED: {
             statusCode: 212,
             success: true,
             customMessage: 'Please provide your Phone Number.Your phone is not verified yet.',
             type: 'PHONE_NOT_VERIFIED'
         },
         PHONE_ALREADY_VERIFIED: {
             statusCode: 213,
             success: true,
             customMessage: 'Your phone number is already verified for this booking. Proceed to confirm.',
             type: 'PHONE_ALREADY_VERIFIED'
         },
         CREATED: {
             success: true,
             statusCode: 201,
             customMessage: 'Created Successfully',
             type: 'CREATED'
         },
         RESET_TOKEN_SENT: {
             success: true,
             statusCode: 200,
             customMessage: 'Reset Email Sent Successfully, Please check your mail.',
             type: 'RESET_TOKEN_SENT'
         },
         DEFAULT: {
             success: true,
             statusCode: 200,
             customMessage: 'Success',
             type: 'DEFAULT'
         },
         UPDATED: {
             success: true,
             statusCode: 200,
             customMessage: 'Updated Successfully',
             type: 'UPDATED'
         },
         LOGOUT: {
             success: true,
             statusCode: 200,
             customMessage: 'Logged Out Successfully',
             type: 'LOGOUT'
         },
         DELETED: {
             success: true,
             statusCode: 200,
             customMessage: 'Deleted Successfully',
             type: 'DELETED'
         },
     }

 }


 var swaggerDefaultResponseMessages = [{
     code: 200,
     message: 'OK'
 }, {
     code: 400,
     message: 'Bad Request'
 }, {
     code: 401,
     message: 'Unauthorized'
 }, {
     code: 404,
     message: 'Data Not Found'
 }, {
     code: 500,
     message: 'Internal Server Error'
 }];


 var smsNotificationMessages = {
     verificationCodeMsg: 'Hi, Your 4 digit verification code for ' + SERVER.APP_NAME + ' is {{verificationCode}}.',
     userRegisterMsg: 'Hi {{userName}}, Thanks for registering with ' + SERVER.APP_NAME + '. You may call at 1800-3000-4243 for any further assistance.'
 }


 var emailNotificationMessages = {
     registrationEmail: {
         emailMessage: "Dear {{user_name}}, <br><br> Please  <a href='{{verification_url}}'>click here</a> to verify your email address",
         emailSubject: "Welcome to " + SERVER.APP_NAME
     },
     forgotPassword: {
         emailMessage: "Hi {{user_name}},<br><br>We have received a request to reset the password for your account.<br><br>If you made this request, please click on the link below or paste this into your browser to complete the process<br><br>{{password_reset_link}}<br><br>This link will work for 1 hour or until your password is reset.<br><br>If you did not ask to change your password, please ignore this email and your account will remain unchanged.<br><br><br>Thanks,<br>Team " + SERVER.APP_NAME + "!<br/>",
         emailSubject: "Reset your password"
     },
 };

 var languageSpecificMessages = {
     verificationCodeMsg: {
         EN: 'Your 4 digit verification code for Eiya Wallet is {{four_digit_verification_code}}',
         ES_MX: 'Los 4 dígitos de verificación para Eiya Wallet son {{four_digit_verification_code}}'
     }
 };

 var APP_CONSTANTS = {
     SERVER: SERVER,
     USER_CONSTANTS: USER_CONSTANTS,
     ADMIN_CONSTANTS: USER_CONSTANTS,
     DATABASE: DATABASE,
     PAGE_LIMIT: PAGE_LIMIT,
     USER_FILTER_DISTANCE: USER_FILTER_DISTANCE,
     STATUS_MSG: STATUS_MSG,
     emailNotificationMessages: emailNotificationMessages,
     languageSpecificMessages: languageSpecificMessages,
     swaggerDefaultResponseMessages: swaggerDefaultResponseMessages,
     smsNotificationMessages: smsNotificationMessages,
     ADMIN_ACCESS_TOKEN: ADMIN_ACCESS_TOKEN,
     NOTIFICATIONS: NOTIFICATIONS
 };

 module.exports = APP_CONSTANTS;
