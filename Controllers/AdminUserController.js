var Service = require('../Services');
var ControllerModule = require('./ControllerModule').ControllerModule;
var UniversalFunctions = require('../Utils/UniversalFunctions');
var controllerHelper = require('./commonControllerFunctions')
var UploadManager = require('../Lib/UploadManager');


function startSection(sectionName) {

    console.log("=====================" + sectionName + "===================")
}

/**
 * '[sixteenPFQuestionController: creating a child controller which inherit all the properties of parant controller]'
 * @param  {[object]} service [passing the ralated service]
 * @return {[null]}         [null]
 */
function adminUserController(service) {

    // binding this service with the service in the parent module i.e. ControllerModule
    ControllerModule.call(this, service);


}

adminUserController.prototype = Object.create(ControllerModule.prototype) // inheritance happening here


adminUserController.prototype.parantDelete = ControllerModule.prototype.delete;



adminUserController.prototype.getUserDetails = function(payloadData, callback) {

    payloadData.projection = {};
    payloadData.options = {};
    this.getFromService(payloadData, function(err, result) {
        if (err) {
            return callback(err);
        } else {
            userFound = result && result[0] || null;
            payloadData.userDetails = userFound.toJSON();

            //console.log(userFound.toJSON())

            callback(null, payloadData);
        }
    });
};




adminUserController.prototype.delete = function(payloadData, callback) {
    startSection("adminUserController.prototype.remove")
    var waterfallArray = [];

    waterfallArray.push(this.getUserDetails.bind(this, payloadData));
    waterfallArray.push(this.parantDelete.bind(this));
    controllerHelper.handleWaterFallFunctions(waterfallArray, callback)
};


module.exports = new adminUserController(Service.AdminUserService);

