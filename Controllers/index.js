

var ControllerModule = require('./ControllerModule').ControllerModule;
var Service = require('../Services');

var objectToExport = {
  	AppVersionController: require('./AppVersionController'),
	AdminModuleController : new ControllerModule(Service.AdminModuleService),
  makeModule: {}
};


for (key in Service.makeModule) {
  objectToExport.makeModule[key] = new ControllerModule(Service.makeModule[key]);

}

objectToExport.makeModule['adminConstants'] = require('../Models/AdminConstants/AdminConstantsController').adminConstants;


objectToExport.makeModule['smsMessages'] = require('../Models/SmsMessages/SmsMessagesController').smsMessages;


objectToExport.makeModule['accessTokens'] = require('../Models/AccessTokens/AccessTokensController').accessTokens;


objectToExport.makeModule['admins'] = require('../Models/Admins/AdminsController').admins;


objectToExport.makeModule['notifications'] = require('../Models/Notifications/NotificationsController').notifications;


objectToExport.makeModule['users'] = require('../Models/Users/UsersController').users;


objectToExport.makeModule['images'] = require('../Models/Images/ImagesController').images;


objectToExport.makeModule['feedback'] = require('../Models/Feedback/FeedbackController').feedback;



// objectToExport.makeModule['powerPlant'] = require('../Models/PowerPlant/PowerPlantController').powerPlant;



objectToExport.makeModule['flagImage'] = require('../Models/FlagImage/FlagImageController').flagImage;


module.exports = objectToExport;