'use strict';

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [

	{
        method: 'GET',
        path: '/v1/notifications',
        handler: function(request, reply) {

         reply(UniversalFunctions.sendSuccess(null, data));

        },
        config: {
            description: 'Get notifications of a User',
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    page: Joi.string().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/v1/notifications/{id}',
        handler: function(request, reply) {

            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var data = {};

            if (userData && userData.id) {
                data.notifId = request.params.id;
                data.userData = userData;
                Controller.NotificationController.markNotificationsViewed(data, function(err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess());
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Get notifications of a User',
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                params: {
                    id: Joi.string()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }

]
