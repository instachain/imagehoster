
'use strict';

var routes = require('./RoutesModule').Routes;
var Controller = require('../Controllers');
var Joi = require('joi');
var UniversalFunctions = require('../Utils/UniversalFunctions');


var joi = {
    string: Joi.string(),
    file: Joi.any().meta({
        swaggerType: 'file'
    }).optional().description('image file'),
    array: Joi.array(),
    number: Joi.number(),
    boolean: Joi.boolean(),
    object: Joi.object(),
    any: Joi.any()
};

var adminUserSchema = {

    put: {
        _id: Joi.string().required(),
        firstName: Joi.string().optional().min(2),
        lastName: Joi.string().optional().min(2),
        countryCode: Joi.string().max(4).optional().trim(),
        email: Joi.string().optional(),
        phone: Joi.number().min(5).optional(),
        isBlocked: Joi.boolean().optional(),
        isDeleted: Joi.boolean().optional(),
        deletedAt: Joi.date().optional(),
        lastBlockedAt: Joi.date().optional()
    },
    post: {}
};




var adminSchema = {

    put: {},
    post: {}
};



// var adminUserRoutes = new routes(Controller.AdminUserController, adminUserSchema, "admins", "users");

// var adminRoutes = new routes(Controller.AdminModuleController, adminSchema, "admins", "");

// var AdminRoute = require('./../Models/Admins/AdminsRoutes');



var all = [];


var emptySchema = {
    put: {},
    post: {}

};

var makeModule = {

};



var templateSchema = {
    put: {
        _id: Joi.string().required(),
        title: Joi.string(),
        subject: Joi.string().optional(),
        body: Joi.string().optional(),
        senderEmail: Joi.string().email().optional(),
        handlebarVars: Joi.array().items(Joi.string().required()),
    },
    post: {
        title: Joi.string().required(),
        subject: Joi.string().required(),
        body: Joi.string().required(),
        senderEmail: Joi.string().email().required(),
        handlebarVars: Joi.array().items(Joi.string().required()),
    }
};


function getSchema(key) {
    var schema = emptySchema;
    if (key == 'emailTemplates') {
        schema = templateSchema;
    }    else if (key == 'coupons') {
        schema = couponSchema;
    }  else {
        schema = emptySchema;
    }

    return schema;
}

var makeModuleSchemas = {};


// autometic generation of modules in make module define in services index

for (key in Controller.makeModule) {


    makeModuleSchemas[key] = getSchema(key);

    makeModule[key] = new routes(Controller.makeModule[key], makeModuleSchemas[key], "admins", key);



       if (key == 'adminConstants') {
 makeModule[key] = require('./../Models/AdminConstants/AdminConstantsRoutes').adminConstants;
}


        else if (key == 'admins') {
 makeModule[key] = require('./../Models/Admins/AdminsRoutes').admins;
}



       else if (key == 'accessTokens') {
 makeModule[key] = require('./../Models/AccessTokens/AccessTokensRoutes').accessTokens;
}



       else if (key == 'editHistories') {
 makeModule[key] = require('./../Models/EditHistories/EditHistoriesRoutes').editHistories;
}


   else if (key == 'notifications') {
 makeModule[key] = require('./../Models/Notifications/NotificationsRoutes').notifications;
}



       else if (key == 'users') {
 makeModule[key] = require('./../Models/Users/UsersRoutes').users;
}



       else if (key == 'feedback') {
 makeModule[key] = require('./../Models/Feedback/FeedbackRoutes').feedback;
}


       else if (key == 'images') {
 makeModule[key] = require('./../Models/Images/ImagesRoutes').images;
}


//        else if (key == 'powerPlant') {
//  makeModule[key] = require('./../Models/PowerPlant/PowerPlantRoutes').powerPlant;
 
// }



       else if (key == 'flagImage') {
 makeModule[key] = require('./../Models/FlagImage/FlagImageRoutes').flagImage;
}


all = all.concat(makeModule[key].getRoutes());
}


all = all.concat(require('./modelRoutes'))


module.exports = all;
