var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');


function handleControllerResponse(err, data, statusMessage) {


    if (!this.request.auth.isAuthenticated) {
        return this.reply('Authentication failed due to: ' + this.request.auth.error.message);
    }

    console.log(err, data, statusMessage);


    if (err == 1) {
        this.reply(UniversalFunctions.sendSuccess(statusMessage, data)).code(statusMessage.statusCode);
    } else {
        if (err) {
            this.reply(UniversalFunctions.sendError(err));
        } else {
            this.reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT, data)).code(201)
        }
    }

}

exports.handleControllerResponse = handleControllerResponse;



function handleControllerResponsePromise(context, fn, data) {


    // if (!this.request.auth.isAuthenticated) {
    //   return this.reply('Authentication failed due to: ' + this.request.auth.error.message);
    // }

    // console.log(err, data, statusMessage);


    // if(err == 1) {
    //   this.reply(UniversalFunctions.sendSuccess(statusMessage, data)).code(statusMessage.statusCode);
    // } else {
    //   if (err) {
    //     this.reply(UniversalFunctions.sendError(err));
    //   } else {
    //     this.reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT, data)).code(201)
    //   }
    // }


    if (data.fileToSend) {
        return new Promise(resolve => {

            fn.call(context, data, (err, data, statusMsg) => {
                console.log("bhej rahe file")

                if (!this.request.auth.isAuthenticated) {
                    console.log(this.request.auth)
                    resolve(this.request.auth.error.message);
                } else {
                    var fileToSend = fs.readFileSync(data.path);
                    resolve(fileToSend)

                }
            });





        });
    } else {
        return new Promise(resolve => {

            fn.call(context, data, (err, data, statusMsg) => {


                if (!this.request.auth.isAuthenticated) {
                    console.log(this.request.auth)
                    resolve(this.request.auth.error.message);
                } else {

                    if (err == 1) {
                        resolve(UniversalFunctions.sendSuccess(statusMessage, data));
                    } else {

                        if (err) {
                            resolve(UniversalFunctions.sendError(err));
                        } else {
                            resolve(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT, data,statusMsg))
                        }

                    }
                }
            });





        });
    }

}

exports.handleControllerResponsePromise = handleControllerResponsePromise;


function handleControllerResponseWithoutAuth(err, data, statusMessage) {
    if (err) {
        this.reply(UniversalFunctions.sendError(err));
    } else {
        this.reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT, data)).code(201)
    }
}

exports.handleControllerResponseWithoutAuth = handleControllerResponseWithoutAuth;



function handleControllerResponseWithoutAuthPromise(context, fn, data) {


    return new Promise(resolve => {

        fn.call(context, data, (err, data, statusMsg) => {

            if (err) {
                resolve(UniversalFunctions.sendError(err));
            } else {
                resolve(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT, data))
            }



        });
    });




}

exports.handleControllerResponseWithoutAuthPromise = handleControllerResponseWithoutAuthPromise;

exports.handleControllerResponseWithoutAuth = handleControllerResponseWithoutAuth;


function handleControllerResponsePost(err, data, statusMessage) {

    console.log(err, data, statusMessage);

    // if (!this.request.auth.isAuthenticated) {

    //   return this.reply('Authentication failed due to: ' + this.request.auth.error.message);
    // }
    if (err) {
        this.reply(UniversalFunctions.sendError(err));
    } else {
        this.reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(200)
    }
}
exports.handleControllerResponsePost = handleControllerResponsePost;


function handleControllerResponseForFile(err, filePath, fileName) {


    if (!this.request.auth.isAuthenticated) {
        return this.reply('Authentication failed due to: ' + this.request.auth.error.message);
    }

    console.log(err, filePath, fileName);

    if (err) {
        this.reply(UniversalFunctions.sendError(err));
    } else {

        var fs = require('fs');

        var fileToSend = fs.readFileSync(filePath);

        var name = 'attachment; filename=' + fileName + ';';

        console.log(' dailyReport RESPONSE: ', filePath, fileName);

        this.reply(fileToSend)
            .bytes(fileToSend.length)
            .type('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            .header('content-disposition', fileName);

    }

}
exports.handleControllerResponseForFile = handleControllerResponseForFile;



var routesPlugin = {
    //'hapiAuthorization': {roles: ['SUPER_DUPER_ADMIN', 'SUPER_ADMIN']},
    'hapi-swagger': {
        // payloadType: 'form',
        responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
    },

};



exports.routesPlugin = routesPlugin;
