/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

var dir = __dirname;
var base = dir.split('/config')[0];
module.exports = {
    webUrl: {
        user: '',
        vendor: '',
        admin: ''
    },
    defaultUrl: {
    },
    HAPI_PORT: 8008,
    mongoUri: "mongodb://localhost:27017/instachain_image_hosting",
    THUMB_HEIGHT: 350,
    THUMB_WIDTH: 350,
    s3Bucket: {
        name: 'instachain-app',
        bucketUrl: 'https://s3-us-east-2.amazonaws.com/instachain-app',
        region: 'us-east-2',
        endpoint: 's3.us-east-2.amazonaws.com',
        keyPairId: 'APKAILAWQBYPTAJKNENQ',
        endpoint: 's3.us-east-2.amazonaws.com',
        accessKeyId: 'AKIAJUDMEB3G3E3J6TDA',
        secretAccessKey: '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',
        cloudfrontUrl: 'http://dp2mjfc5y6xi1.cloudfront.net/',
        privateKey: "-----BEGIN RSA PRIVATE KEY-----\nMIIEpgIBAAKCAQEAl6xm/yhlDzepTcDZsm0VHrLSp5EFFIZMFHk+gYrzQHZR9Ge0\n5e1g691fb4TobPTu5pmt02zcQaASoiyouvPind6tND5788P8NMLh4syhY6lpmY5H\nMnXtEpwJvApUnOB0ABvI6wlGOmo0BzFrhRSV4XSazPyfMGqH3k87GpF4Qtf9DuF0\nOcDW73iaN3PHaI0iJXq0dBQD3Lanb4DOWu0aCez6KaCfNwXDOQ7oYpsZdJS3nsic\nqOUfvWRcWQ9G1BCkMx1hRfySdZ8PEj3Cc8Gt9arxdCbTndyz+vo+mEWlQVdgWjtq\nj604RT7QwU71o/MieCicWIhHynjyr/cZQa5GGwIDAQABAoIBAQCN/FejD4ajm0DD\nyABDlvhJd6/aK9ksoJgCJopMLO6Q7vLDsH3ZLMIpq1/W/v2yWgx3FXBzVxpN2PL4\nnIQMF5mSnk1YdhIFXffw0GszrMp8iLCRjm66H0kiva4jOSyCe2A8ZcZNuK3kq10v\nSqVTvLSejVEJCohfg6rTgypaMcn6D9UZzqUslWW87URACdbWCp9U1bgu/SotxWl9\nm6kP7x2SxelB4BhZrW+kBxJpBmU4DQijlKSplveiTnnfH/7IqrgoAGKAR9tTulAL\nysj9DO8bPo55MCbBwtwiQa53ctcLoUU70O1QcXlcWsuMPiHuJhI3HLRcXYkB+wb1\ntHB8upKxAoGBAMiO0IcyO+3uMmNGl4sPqRX9vHJehLrFsKWnpoK9QjhT+Vr/rutu\nwvFres/PAdsqVSQPZ0YWOfINyQR6B4OFK0gDAY4L9Jqa2mmAyvK9ue2sIZqMG1V8\nmwYyhfO4l/0gLqQSCdBL8RElbmgBsmQxV9WMcV1fjKwwcLtl5slen9bjAoGBAMGa\nG+vLFVsG8emfmKv1m4p4OvwM93XxUyFNHsEWdmuU7lORVpoGayoGVS6lR3lOl1E6\nZLiihKagbiGU7Gx3lgvDpjpIEiBOeptrcKFE+8dp2wRt/7NqcX6PWfsC/Z/sjFr4\nSBBSUcl9RhnUcF2r6AOlrm1UxCe053olRoK2ysFpAoGBAJOY/wAwSQiOn0iOgCKY\nUODdTp2Az//AU4NmQVXAQQbBTibZOnsLla4kYkqjVwNbQTN7no5m9xgbaw+/Wt5A\n2Bx0EeXfCJHrWsgAP9urIzU107kwWUFtpY1HC2vVMw3XbFvU5qID8ZKj0H/8l7PC\n2JhRdnC4TAfA0oYntVQHLmzvAoGBAK3YVZQhU3bewKHZdv2K2XW4yraEaj2VSuGP\nX7thEUs/e9a8nnhGpToiX6E4Nvsd8FC3T7Rf8x+w1dP/52Dq7Wh48i5Jg0VRU1V2\nX7fguvnw+wN6Ru7z+R/9V35HzDkr70jMw+EsGlcEz8rn8VKbyAZmYz+3q0BAeM88\n45kCzQRpAoGBALzfSQOqhhnnyJAtw8bpE96rFz/p0w5syvqSpI9L05K+Co7exxwY\n0Lv/aAx9Ik/0PnxGZSIGRe/khwOL84Kndv9M1u/cIs/Ds7CMfL60SxRzdFCRLEZf\ndcA18hew6VmZLIQCdms6pKYEWBOYeU4ZSTTWci5I9R5s1iVkU52fMdCj\n-----END RSA PRIVATE KEY-----"
    },
    golos: {
        webSocket: "ws://network.instachain.io:8090",
        addressPrefix: "INS",
        chainId: "e9612c0b8ec8206dfcd10a12bf28be747d6b62d536306df0d68978993d13c823"
    },

    JWT_SECRET_KEY: "sUPerSeCuREKeY$$BASe$$&^$^&$^%C$^%34Ends",
};