'use strict';

var mongoose = require('mongoose');
var Config = require('../constant');
var Service = require('../Services');
var async = require('async');
var DataTable = require('mongoose-datatable');
var Models = require('../Models');
var config = require('config');
mongoose.Promise = global.Promise;
require('./initialData');

//Connect to MongoDB

// var mongoUri = 'mongodb://' + process.env.
// var dbConfig = 'mongodb://' + config.get('DATABASE_USER') + ':' + config.get('DATABASE_PASSWORD') + '@' + config.get('DATABASE_HOST') + ':' + config.get('DATABASE_PORT') + '/' + config.get('DATABASE_NAME');
// var dbConfig = 'mongodb://' + config.get('DATABASE_HOST') + ':' + config.get('DATABASE_PORT') + '/' + config.get('DATABASE_NAME');

var dbConfig = config.get('mongoUri');

// mongoose.connect(Config.dbConfig.mongo.URI, function(err) {
mongoose.connect(dbConfig, { useMongoClient: true }, function(err) {
    if (err) {
        console.log('Mongo Connection Error : ',err);
        process.exit(1);
    }
});

DataTable.configure({ debug: false, verbose: false });
mongoose.plugin(DataTable.init);

exports.bootstrapAdmin = function(callback) {

    callback(null, 'Bootstrapping finished');
};

exports.bootstrapAppVersion = function(callback) {
    var appVersion1 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.userType.User
    };

    async.parallel([
        function(cb) {
            //insertVersionData(appVersion1.appType, appVersion1, cb)
        },
        function(cb) {
            //  insertVersionData(appVersion2.appType, appVersion2, cb)
        }
    ], function(err, done) {
        callback(err, 'Bootstrapping finished For App Version');
    })
};

function insertVersionData(appType, versionData, callback) {
    var needToCreate = true;
    async.series([
        function(cb) {
            var criteria = {
                appType: appType
            };
            Service.AppVersionService.getAppVersion(criteria, {}, {}, function(err, data) {
                if (data && data.length > 0) {
                    needToCreate = false;
                }
                cb()
            })
        },
        function(cb) {
            if (needToCreate) {
                Service.AppVersionService.createAppVersion(versionData, function(err, data) {
                    cb(err, data)
                })
            } else {
                cb();
            }
        }
    ], function(err, data) {
        callback(err, 'Bootstrapping finished For Admin Data')
    })
}




function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function(cb) {
        var criteria = {
            email: email
        };
        Service.makeModule.admins.view(criteria, {}, {}, function(err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
                Config.APP_CONSTANTS.ADMIN_ACCESS_TOKEN = data[0].accessToken;
            }
            cb()
        })
    }, function(cb) {
        if (needToCreate) {
            Service.makeModule.admins.add(adminData, function(err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function(err, data) {

        callback(err, 'Bootstrapping finished')
    })
}



exports.createApiPaths = function(pathArr) {

    var criteria = {};
    var options = { upsert: true }

    createSuperDuperAdminRole(function(err, result) {

        pathArr.forEach(function(route) {

            if (Service.makeModule.apiPaths) {
                Service.makeModule.apiPaths.edit({ method: route.method, path: route.path }, { method: route.method, path: route.path }, options, function(err, result) {

                })
            }
        });
    })
}

exports.createOrgApiPaths = function(pathArr) {

    var criteria = {};
    var options = { upsert: true }
    createSuperDuperAdminRole(function(err, result) {
        pathArr.forEach(function(route) {

            if (Service.makeModule.apiPaths) {
                Service.makeModule.orgAuthApis.edit({ method: route.method, path: route.path, name: route.method + ":" + route.path }, { method: route.method, path: route.path }, options, function(err, result) {
                })
            }
        });
    })
}

var crudPermission = ['READ', 'ADD', 'EDIT', 'DELETE']

exports.createPermissionsForEachRole = function createPermissionsForEachRole() {

    if (Service.makeModule.roles) {
        Service.makeModule.roles.view({}, {}, {}, function(err, result) {

            result.forEach(function(role) {

                Object.keys(Service.makeModule).forEach(function(model) {

                    crudPermission.forEach(function(operation) {

                        Service.makeModule.accessControlList.edit({ roleId: role._id, model: model, isAllow: false, permission: operation }, { roleId: role._id, model: model, permission: operation }, { upsert: true, new: true }, function(err, result) {
                        })
                    })
                })
            })
        })
    }
}

function createSuperDuperAdminRole(callback) {
    if (Service.makeModule.roles) {
        Service.makeModule.roles.edit({ name: "SUPER_ADMIN" }, { name: "SUPER_ADMIN" }, { upsert: true, new: true }, callback)
    }
}

function createNewAdminConstant() {
    Service.makeModule.adminConstants.add({}, {}, {}, function(err, result) {

    })
}