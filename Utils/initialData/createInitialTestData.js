var Service = require('../../Services');
var advancedFunctions = require('../commonAdvanceFunctions');

var parallel = advancedFunctions.handleParallelFunctions;
var waterfall = advancedFunctions.handleWaterFallFunctions;


function createTestType(type) {
  this.testType = type;
  this.isTestExists = false;
  this.service = Service.makeModule.testDetails;

  console.log('Bootstrapping started for ' + type);
}


 function checkTest(callback) {
  var criteria = {
    type: this.testType
  };
  var that = this;
  this.service.view(criteria, {}, {}, function(err, data) {


    if (data.length > 0) {
    
      that.isTestExists = true;
    }
    callback(err);

  });

};

createTestType.prototype.checkTest = checkTest;
 function addTest(callback) {
  var addData = {
    type: this.testType
  };
  if (!this.isTestExists) {
    this.service.add(addData, function(err, data) {
      callback(err);
    });
  } else {
    process.nextTick(callback.bind(null, null,"done"));
  }


};
createTestType.prototype.addTest = addTest;

 function checkAndAddTest(callback) {

    var that = this;

  var waterfallArray = [];

  waterfallArray.push(this.checkTest.bind(this));
  waterfallArray.push(this.addTest.bind(this));
  waterfall(waterfallArray, function(err, result) {
    console.log('Bootstrapping finished for ' + that.testType);
    callback(err, result);
  });

};



createTestType.prototype.checkAndAddTest = checkAndAddTest;

function createPredefinedTestsDetails(tests) {

  this.tests = tests;
  var parallelArray = [];

  var tests = {};

  this.tests.forEach(function(test) {

    tests[test] = new createTestType(test);

    parallelArray.push(tests[test].checkAndAddTest.bind(tests[test]));
  });



console.log(parallelArray);
  parallel(parallelArray, function(err, result) {
    delete tests;

    console.log('Bootstrapping finished for all tests');

  });


}


module.exports = createPredefinedTestsDetails;