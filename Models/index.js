
module.exports = {

 'Users': require('./Users/Users')
    
,'Admins': require('./Admins/Admins')
    
,'AppVersions': require('./AppVersions')
    
,'Notifications': require('./Notifications/Notifications')
    
,'EmailTemplates': require('./EmailTemplates/EmailTemplates')
    
,'ApiHistories': require('./ApiHistories/ApiHistories')

,'AdminConstants': require('./AdminConstants/AdminConstants')

,'SmsMessages': require('./SmsMessages/SmsMessages')

, 'Feedback' : require('./Feedback/Feedback')

, 'AccessTokens' : require('./AccessTokens/AccessTokens')

, 'EditHistories' : require('./EditHistories/EditHistories')

, 'Images' : require('./Images/Images')

// , 'PowerPlant' : require('./PowerPlant/PowerPlant')

, 'FlagImage' : require('./FlagImage/FlagImage')

};

