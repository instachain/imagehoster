var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');
var ImageControllerHelper = require('../Images/ImageControllerHelper');
var UserControllerHelper = require('../Users/userControllerHelper');


function startSection(sectionName) {

   console.log('=====================' + sectionName + '===================')
}


function FlagImageController(service) {

 
   //console.log('============================================FlagImageController controller initialised')
   ControllerModule.call(this, service);
}

FlagImageController.prototype = Object.create(ControllerModule.prototype)


FlagImageController.prototype.add = function add(data, callback) {

 var waterfallArray = [];

    waterfallArray.push(UserControllerHelper.getUserProfileDataFromBlockChain.bind(null, data));

    waterfallArray.push(UserControllerHelper.verifyBlockchainUser);

    waterfallArray.push(UserControllerHelper.getUserIdforBlockChainUserName);

    waterfallArray.push(ImageControllerHelper.getImageFileUrl);

    waterfallArray.push(ImageControllerHelper.checkIfUserAllowedToSeeTheFile);

    waterfallArray.push(ImageControllerHelper.addTheFlagData);
    
    controllerHelper.handleWaterFallFunctions(waterfallArray, callback);
}

module.exports = {
   'flagImage': new FlagImageController(Service.makeModule.flagImage)
};;