var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

var payload = {
    maxBytes: 20000000,
    parse: true,
    output: 'file',
}

var schema = {
    put: {

    },
    post: {

    }
}

function FlagImageRoute(controller, requestSchema, mainKey, subKey, payload) {

    // binding this controller with the controller in the p arent module i.e. routesModule
    routesModule.call(this, controller, requestSchema, mainKey, subKey, payload);
}

FlagImageRoute.prototype = Object.create(routesModule.prototype) // inheritance happening

FlagImageRoute.prototype.getParentRoutes = FlagImageRoute.prototype.getRoutes;

FlagImageRoute.prototype.post = function(request, reply) {

    var data = {
        payload: request.payload,
    };

    return commonRoutes.handleControllerResponseWithoutAuthPromise.call({
        reply: reply,
        request: request
    }, this.controller,this.controller.add, data);

}


FlagImageRoute.prototype.getRoutes = function(request, reply) {

    var seperator = '';
    if (this.apiName) {
        seperator = '/'
    }

    var newRoutes = [{
        method: 'POST',
        path: '/v1/flagImage',
        handler: this.post.bind(this),
        config: {
            validate: {
                payload: {
                    userName: Joi.string().required(),
                    postingKey: Joi.string().required(),
                    fileId: Joi.objectId().required(),
                    notes: Joi.string().optional(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'flag a file image',
            tags: ['api', 'user'],
            plugins: commonRoutes.routesPlugin
        }
    }]


    return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
    'flagImage': new FlagImageRoute(Controller.makeModule.flagImage, schema, 'admins', 'flagImage')
};