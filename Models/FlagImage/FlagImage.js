var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
// {type: [Number , String , Date] , trim: true, required: true, unique: true , default: Date.now}
var lastMod = require('../lastModPlugin');

var FlagImage = new Schema({
    notes: { type: String, default: null },
    imageId: {
        type: Schema.ObjectId,
        ref: 'Images',
        required: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'Users',
        required: true
    },
    isDeleted: { type: Boolean, default: false }
}, {
    timestamps: true
});


module.exports = mongoose.model('FlagImage', FlagImage);