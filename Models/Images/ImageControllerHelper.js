var async = require('async');
var request = require('request');
var jwt = require('jwt-simple');
var Service = require('../../Services');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var UploadManager = require('../../Lib/UploadManager');
var CodeGenerator = require('../../Lib/CodeGenerator');
var Config = require('../../constant');
var config = require('config');
var UserControllerHelper = require('../Users/userControllerHelper');

var createSignedInUrl = function(data, callback) {

    UploadManager.getSignedUrl(data.imageObj.url, 180, function(err, fileUrl) {

        if (err) {
            return callback(err);
        } else {
            UploadManager.getSignedUrl(data.imageObj.thumbUrl, 180, function(err, thumbUrl) {

                if (err) {
                    return callback(err);
                } else {

                    let obj = {
                        original: fileUrl,
                        thumb: thumbUrl
                    }

                    return callback(null, obj);
                }
            });
        }
    });
};

exports.createSignedInUrl = createSignedInUrl;

var uploadFileOnS3withThumbnail = function(data, callback) {

    UploadManager.uploadImageFileToS3BucketWithThumbnail(data.payload.file, data.user._id, function(err, fileData) {

        if (err) {
            return callback(err);
        } else {
            data.fileUrl = config.get('s3Bucket').cloudfrontUrl + data.user._id + '/' + fileData.original;
            data.thumbUrl = config.get('s3Bucket').cloudfrontUrl + data.user._id + '/' + fileData.thumbnail;
            return callback(null, data);
        }
    });
};

exports.uploadFileOnS3withThumbnail = uploadFileOnS3withThumbnail;


var uploadFileOnS3withThumbnailByAdmin = function(data, callback) {

    UploadManager.uploadImageFileToS3BucketWithThumbnail(data.payload.file, data.adminData._id, function(err, fileData) {

        if (err) {
            return callback(err);
        } else {
            data.fileUrl = config.get('s3Bucket').cloudfrontUrl + data.adminData._id + '/' + fileData.original;
            data.thumbUrl = config.get('s3Bucket').cloudfrontUrl + data.adminData._id + '/' + fileData.thumbnail;
            return callback(null, data);
        }
    });
};

exports.uploadFileOnS3withThumbnailByAdmin = uploadFileOnS3withThumbnailByAdmin;


var uploadFileOnS3 = function(data, callback) {

    UploadManager.uploadFileToS3Bucket(data.payload.file, data.user._id, function(err, fileData) {

        if (err) {
            return callback(err);
        } else {
            data.fileUrl = config.get('s3Bucket').cloudfrontUrl + data.user._id + '/' + fileData;
            // data.fileUrl = config.get('s3Bucket').cloudfrontUrl + fileData;
            return callback(null, data);
        }
    });
};

exports.uploadFileOnS3 = uploadFileOnS3;


var checkfileTagInDb = function(data, callback) {

    if (!data.payload.tag) {
        savefileDataInDb(data, callback)
    } else {
        var criteria = {
            tag: data.payload.tag,
            userId: data.user._id
        }

        var projection = {};
        var options = {};

        Service.makeModule.images.view(criteria, projection, options, function(err, result) {

            if (err) {
                return callback(err);
            } else if (result.length) {
                data.imageObj = result[0];
                updateFileUrlInDB(data, callback)

            } else {
                savefileDataInDb(data, callback)
            }
        });
    }
};

exports.checkfileTagInDb = checkfileTagInDb;


var updateFileUrlByAdmin = function(data, callback) {

   var criteria = {
        _id: data.params._id
    }
    var dataToSet = {
        $set:{
            alternateUrl: data.fileUrl,
            alternateThumbUrl: data.thumbUrl,
            warning: true,
        }
    };
    var options = {
        new: true
    };

    Service.makeModule.images.edit(criteria, dataToSet, options, function(err, result) {

        if (err) {
            return callback(err);
        } else {
            callback(null, result._id);
        }
    });

};

exports.updateFileUrlByAdmin = updateFileUrlByAdmin;

var addTheFlagData = function(data, callback) {

    var insertData = {
        imageId: data.payload.fileId,
        notes: data.payload.notes,
        userId: data.user._id,
    }

    Service.makeModule.flagImage.add(insertData, function(err, result) {

        if (err) {
            return callback(err);
        } else {
            callback(null, "Success");
        }
    });
};

exports.addTheFlagData = addTheFlagData;


function updateFileUrlInDB(data, callback) {

    var criteria = {
        tag: data.payload.tag,
        userId: data.user._id
    }
    var dataToSet = {
        url: data.fileUrl,
        thumbUrl: data.thumbUrl,
    };
    var options = {
        new: true
    };

    Service.makeModule.images.edit(criteria, dataToSet, options, function(err, result) {

        if (err) {
            return callback(err);
        } else {
            callback(null, result._id);
        }
    });

}


function savefileDataInDb(data, callback) {


    var insertData = {
        url: data.fileUrl,
        thumbUrl: data.thumbUrl,
        tag: data.payload.tag,
        userId: data.user._id,
    }

    Service.makeModule.images.add(insertData, function(err, result) {

        if (err) {
            return callback(err);
        } else {
            callback(null, result._id);
        }
    });
}

var getImageFileUrl = function(data, callback) {

    var criteria = {
        _id: data.payload.fileId,
        population: [{ path: "userId" }]
    }

    var projection = {};
    var options = {};

    Service.makeModule.images.view(criteria, projection, options, function(err, result) {

        if (err) {
            return callback(err);
        } else if (result.length) {
            data.imageObj = result[0];

            console.log(data.imageObj);
            return callback(null, data);

        } else {
            return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.CANT_FIND);
        }
    });
};

exports.getImageFileUrl = getImageFileUrl;

var getImagesList = function(data, callback) {

    var criteria = {
        isDeleted: {
            $ne: true
        }
    };

    var projection = {};

    var limit = data.payload.count;
    var skip = limit * (data.payload.page - 1);
    
    var options = {
        lean: true,
        skip: skip,
        limit: limit,
    };

    Service.makeModule.images.view(criteria, projection, options, function(err, result) {

        if (err) {
            return callback(err);
        } else {
            data.images = result;
            data.total = result.length;
            return callback(null, data);
        } 
    });
};

exports.getImagesList = getImagesList;

var getTotalImagesCount = function(payloadData, callback) {

    var criteria = {
        isDeleted: false,
    };

    Service.makeModule.images.count(criteria,function(err, total) {
        if (err) {
            return callback(err);
        } else {
            payloadData.extraData.total = total;
            return callback(null, payloadData);
        }
    });
}

exports.getTotalImagesCount = getTotalImagesCount;


var getImagesSignedUrl = function(payloadData, callback) {

    async.map(payloadData.images, function(obj, cb) {
        signedUrl(obj,cb);
    }, function(err, results) {
        callback(null, payloadData);
    })
}

exports.getImagesSignedUrl = getImagesSignedUrl;

function signedUrl(obj,callback){

    UploadManager.getSignedUrl(obj.url, 180, function(err, fileUrl) {

        if (err) {
            return callback(err);
        } else {
            UploadManager.getSignedUrl(obj.thumbUrl, 180, function(err, thumbUrl) {

                if (err) {
                    return callback(err);
                } else {
                    obj.url = fileUrl;
                    obj.thumbUrl = thumbUrl;
                    return callback(null, obj);
                }
            });
        }
    });
}

var checkIfUserAllowedToSeeTheFile = function(data, callback) {


    if (data.imageObj.userId.isPrivate === false || data.imageObj.tag) {
        console.log('************** checkIfUserAllowedToSeeTheFile 1 *****************');

        return callback(null, data);
    } else if (data.imageObj.userId._id.toString() === data.user._id.toString()) {

        console.log('************** checkIfUserAllowedToSeeTheFile 2 *****************');

        return callback(null, data);
    } else {
        console.log('************** checkIfUserAllowedToSeeTheFile 3 *****************');

        UserControllerHelper.checkifFollowUser(data, callback);
    }
};

exports.checkIfUserAllowedToSeeTheFile = checkIfUserAllowedToSeeTheFile;