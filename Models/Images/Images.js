var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
var lastMod = require('../lastModPlugin');

var Images = new Schema({
    url: { type: String, trim: true },
    thumbUrl: { type: String, trim: true },

    alternateUrl: { type: String, trim: true },
    alternateThumbUrl: { type: String, trim: true },

    tag: { type: String, trim: true, default: null },
    isDeleted: { type: Boolean, default: false },
    warning: { type: Boolean, default: false },
    userId: {
        type: Schema.ObjectId,
        ref: 'Users'
    },
}, {
    timestamps: true
});


module.exports = mongoose.model('Images', Images);