var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

var payload = {
    maxBytes: 20000000,
    parse: true,
    output: 'file',
    allow: 'multipart/form-data',
    timeout: false
}

var schema = {
    put: {

        _id: Joi.string(),
        // tags : Joi.string(),
        title: Joi.string(),
        imageFile: Joi.any()
            .meta({ swaggerType: 'file' })
            .required()
            .description('image file')

    },
    post: {

        // departmentName : Joi.string(),
        title: Joi.string(),
        imageFile: Joi.any()
            .meta({ swaggerType: 'file' })
            .required()
            .description('image file')

    }
}


function ImagesRoute(controller, requestSchema, mainKey, subKey, payload) {

    // binding this controller with the controller in the p arent module i.e. routesModule
    routesModule.call(this, controller, requestSchema, mainKey, subKey, payload);
}

ImagesRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


ImagesRoute.prototype.getParentRoutes = ImagesRoute.prototype.getRoutes;
//ImagesRoute.prototype.overridedParantFunction = ImagesRoute.prototype.ParantFunction;

ImagesRoute.prototype.uploadFile = function(request, reply) {

    var data = {
        payload: request.payload
    };
    return commonRoutes.handleControllerResponseWithoutAuthPromise(this.controller, this.controller.uploadFile, data);
}

ImagesRoute.prototype.getFileFromId = function(request, reply) {

    var data = {
        payload: request.query
    };
    return commonRoutes.handleControllerResponseWithoutAuthPromise(this.controller, this.controller.getFileFromId, data);
}

ImagesRoute.prototype.table = function(request, reply) {

    var adminData = request.auth && request.auth.credentials && request.auth.credentials.adminData || null;

    if (!adminData) {
        return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
    }
    var data = {
        payload: request.query,
        adminData: adminData,
    };

    data.extraData = {
        perPage: request.query.perPage,
        page: request.query.page,
        total: 0
    };

    return commonRoutes.handleControllerResponsePromise.call({
        reply: reply,
        request: request
    }, this.controller, this.controller.table, data);
}

ImagesRoute.prototype.replaceImage = function(request, reply) {


    var adminData = request.auth && request.auth.credentials && request.auth.credentials.adminData || null;

    if (!adminData) {
        return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
    }
    var data = {
        payload: request.payload,
        params: request.params,
        adminData: adminData,
    };

    return commonRoutes.handleControllerResponsePromise.call({
        reply: reply,
        request: request
    }, this.controller, this.controller.replaceImage, data);
}

ImagesRoute.prototype.deleteById = function(request, reply) {


    var adminData = request.auth && request.auth.credentials && request.auth.credentials.adminData || null;

    if (!adminData) {
        return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
    }
    var data = {
        params: request.params,
        adminData: adminData,
    };

    return commonRoutes.handleControllerResponsePromise.call({
        reply: reply,
        request: request
    }, this.controller, this.controller.deleteById, data);
}

ImagesRoute.prototype.getRoutes = function(request, reply) {

    var seperator = '';
    if (this.apiName) {
        seperator = '/'
    }

    var newRoutes = [{
            method: 'POST',
            path: '/v1/image/upload',
            handler: this.uploadFile.bind(this),
            config: {
                payload: payload,
                validate: {
                    payload: {
                        userName: Joi.string().required(),
                        postingKey: Joi.string().required(),
                        tag: Joi.string().optional(),
                        file: Joi.any()
                            .meta({
                                swaggerType: 'file'
                            })
                            .required()
                            .description('Image file')
                    },
                    failAction: UniversalFunctions.failActionFunction
                },
                description: 'upload a file',
                tags: ['api', 'user'],
                plugins: commonRoutes.routesPlugin
            }
        }, {
            method: 'GET',
            path: '/v1/image',
            handler: this.getFileFromId.bind(this),
            config: {
                validate: {
                    query: {
                        userName: Joi.string().required(),
                        postingKey: Joi.string().required(),
                        fileId: Joi.objectId().required()
                    },
                    failAction: UniversalFunctions.failActionFunction
                },
                description: 'get a file url',
                tags: ['api', 'user'],
                plugins: commonRoutes.routesPlugin
            }
        },
        {
            method: 'PUT',
            path: '/admin/images/{_id}/replace',
            handler: this.replaceImage.bind(this),
            config: {
                auth: 'AdminAuth',
                payload: payload,
                validate: {
                    headers: UniversalFunctions.authorizationHeaderObj,
                    params: {
                        _id: Joi.objectId().required(),
                    },
                    payload: {
                        file: Joi.any()
                            .meta({
                                swaggerType: 'file'
                            })
                            .required()
                            .description('Image file')
                    },
                    failAction: UniversalFunctions.failActionFunction
                },
                description: 'upload a file',
                tags: ['api', 'admin'],
                plugins: commonRoutes.routesPlugin
            }
        },
        {
            method: 'GET',
            path: '/admin/images',
            handler: this.table.bind(this),
            config: {
                auth: 'AdminAuth',
                validate: {
                    headers: UniversalFunctions.authorizationHeaderObj,
                    query: {
                        search: Joi.string().optional(),
                        sortKey: Joi.string().optional(),
                        sortOrder: Joi.number().optional(),
                        page: Joi.number().required(),
                        perPage: Joi.number().required(),
                    },
                    failAction: UniversalFunctions.failActionFunction
                },
                description: 'image table',
                tags: ['api', 'admin'],
                plugins: commonRoutes.routesPlugin
            }
        },
        {
            method: 'DELETE',
            path: '/admin/images/{_id}',
            handler: this.deleteById.bind(this),
            config: {
                auth: 'AdminAuth',
                validate: {
                    headers: UniversalFunctions.authorizationHeaderObj,
                     params: {
                        _id: Joi.objectId().required(),
                    },
                    params: {
                        _id: Joi.objectId().required()
                    },
                    failAction: UniversalFunctions.failActionFunction
                },
                description: 'Delete an image',
                tags: ['api', 'admin'],
                plugins: commonRoutes.routesPlugin
            }
        },
    ]

    return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
    'images': new ImagesRoute(Controller.makeModule.images, schema, 'admins', 'images', payload)
};