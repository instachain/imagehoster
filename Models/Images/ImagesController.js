var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');
var ImageControllerHelper = require('./ImageControllerHelper');
var UserControllerHelper = require('../Users/userControllerHelper');


function startSection(sectionName) {

   console.log('=====================' + sectionName + '===================')
}

function ImagesController(service) {

   this.imageUpload = true;
   this.imageFile = "imageFile";
   this.imageURL = "image"
   this.isAdvancedImageObject = true;

   ControllerModule.call(this, service);
}

ImagesController.prototype = Object.create(ControllerModule.prototype)

ImagesController.prototype.uploadFile = function uploadFile(data, callback) {

 var waterfallArray = [];

    waterfallArray.push(UserControllerHelper.getUserProfileDataFromBlockChain.bind(null, data));
    waterfallArray.push(UserControllerHelper.verifyBlockchainUser);
    waterfallArray.push(UserControllerHelper.getUserIdforBlockChainUserName);
    // waterfallArray.push(ImageControllerHelper.uploadFileOnS3);
    waterfallArray.push(ImageControllerHelper.uploadFileOnS3withThumbnail);
    waterfallArray.push(ImageControllerHelper.checkfileTagInDb);

    controllerHelper.handleWaterFallFunctions(waterfallArray, callback);
}

ImagesController.prototype.table = function table(data, callback) {

 var waterfallArray = [];

    waterfallArray.push(ImageControllerHelper.getImagesList.bind(null, data));
    waterfallArray.push(ImageControllerHelper.getTotalImagesCount);
    waterfallArray.push(ImageControllerHelper.getImagesSignedUrl);

    controllerHelper.handleWaterFallFunctions(waterfallArray, function(err,result){

      if(err){
        return callback(err);
      }else{
        return callback(null,result.images,result.extraData);
      }
    });
}

ImagesController.prototype.replaceImage = function deleteById(data, callback) {
 var waterfallArray = [];

    waterfallArray.push(ImageControllerHelper.uploadFileOnS3withThumbnailByAdmin.bind(null, data));
    waterfallArray.push(ImageControllerHelper.updateFileUrlByAdmin);

    controllerHelper.handleWaterFallFunctions(waterfallArray, callback);
}

ImagesController.prototype.deleteById = function deleteById(data, callback) {

 data.criteria = { _id: data.params._id };

    data.setData = {
        '$set': {
            isDeleted: true
        }
    };
    this.service.edit(data.criteria, data.setData, { new: true }, callback);
}

ImagesController.prototype.getFileFromId = function getFileFromId(data, callback) {

 var waterfallArray = [];

    waterfallArray.push(UserControllerHelper.getUserProfileDataFromBlockChain.bind(null, data));
    waterfallArray.push(UserControllerHelper.verifyBlockchainUser);
    waterfallArray.push(UserControllerHelper.getUserIdforBlockChainUserName);
    waterfallArray.push(ImageControllerHelper.getImageFileUrl);
    waterfallArray.push(ImageControllerHelper.checkIfUserAllowedToSeeTheFile);
    waterfallArray.push(ImageControllerHelper.createSignedInUrl);
    
    controllerHelper.handleWaterFallFunctions(waterfallArray, callback);
}


module.exports = {
   'images': new ImagesController(Service.makeModule.images)
};;
