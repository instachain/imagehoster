var async = require('async');
var request = require('request');
var jwt = require('jwt-simple');
var Service = require('../../Services');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var TokenManager = require('../../Lib/TokenManager');
var UploadManager = require('../../Lib/UploadManager');
var CodeGenerator = require('../../Lib/CodeGenerator');
var constant = require('../../constant');
var config = require('config');
var golos = require('../../golos-js/lib/index.js');

golos.config.set('websocket', config.get('golos').webSocket); // assuming websocket is work at ws.golos.io
golos.config.set('address_prefix', config.get('golos').addressPrefix);
golos.config.set('chain_id', config.get('golos').chainId);

var getUserProfileDataFromBlockChain = function(data, callback) {

    var usernames = [data.payload.userName];

    golos.api.getAccounts(usernames, function(err, result) {

        if (err) {
            callback(err);
        } else if (result && result.length && result[0].id && result[0].posting.key_auths && result[0].posting.key_auths.length && result[0].posting.key_auths[0].length) {



            console.log('********** getUserProfileDataFromBlockChain *******************',usernames,result[0].posting.key_auths);

            data.pubWif = result[0].posting.key_auths[0][0];
            data.instaChainUserId = result[0].id;
            data.userName = data.payload.userName;
            callback(null, data);
        } else {
            return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.CANT_FIND);
        }
    });
}
exports.getUserProfileDataFromBlockChain = getUserProfileDataFromBlockChain;


var checkifFollowUser = function(data, callback) {

    golos.api.getFollowers(data.imageObj.userId.userName, data.user.userName, 'blog', 100, function(err, result) {

        if (err) {
            return callback(err);
        } else if (result.length) {

            let follwers = [];
            let isFollower = 0;

            result.forEach(function(obj) {

                if (obj.follower == data.user.userName) {
                    isFollower = 1;
                }
            });

            if (isFollower) {
                checkifFollowingUser(data, callback);
            } else {
                return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_AUTHORIZED_TO_VIEW_IMAGE);
            }
        } else {
            return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_AUTHORIZED_TO_VIEW_IMAGE);
        }
    });
}
exports.checkifFollowUser = checkifFollowUser;


var checkifFollowingUser = function(data, callback) {

    golos.api.getFollowers(data.user.userName, data.imageObj.userId.userName, 'blog', 100, function(err, result) {

        if (err) {
            return callback(err);
        } else if (result.length) {

            let follwers = [];
            let isFollower = 0;

            result.forEach(function(obj) {

                if (obj.follower == data.imageObj.userId.userName) {
                    isFollower = 1;
                }
            });
            if (isFollower) {
                return callback(null, data);
            } else {
                return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_AUTHORIZED_TO_VIEW_IMAGE);
            }

        } else {
            return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_AUTHORIZED_TO_VIEW_IMAGE);
        }
    });
}




var verifyBlockchainUser = function(data, callback) {

    try {
        var resultWifIsValid = golos.auth.wifIsValid(data.payload.postingKey, data.pubWif);

        if (resultWifIsValid) {
            return callback(null, data);
        } else {
            return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_POSTING_KEY);
        }

    } catch (e) {
        return callback(constant.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_POSTING_KEY);
    }
}
exports.verifyBlockchainUser = verifyBlockchainUser;



var getUserIdforBlockChainUserName = function(data, callback) {

    var criteria = {
        instaChainUserId: data.instaChainUserId,
        userName: data.userName,
    }

    var projection = {}

    var options = {
        lean: true
    }

    Service.makeModule.users.view(criteria, projection, options, function(err, result) {

        if (err) {
            return callback(err);
        } else if (result.length) {
            data.user = result[0]
            callback(null, data);
        } else {

            var insertData = {
                instaChainUserId: data.instaChainUserId,
                userName: data.userName,
                isPrivate: false
            }
            Service.makeModule.users.add(insertData, function(err, result) {

                if (err) {
                    return callback(err);
                } else {
                    data.user = result
                    callback(null, data);
                }
            });
        }
    });
}
exports.getUserIdforBlockChainUserName = getUserIdforBlockChainUserName;

var getProfileDetailsOfOtherUser = function(data, callback) {

    var criteria = {
        userName: data.payload.otherUserName,
    }

    var projection = {}

    var options = {
        lean: true
    }

    Service.makeModule.users.view(criteria, projection, options, function(err, result) {

        if (err) {
            return callback(err);
        } else if (result.length) {
            let obj = {
                isPrivate :result[0].isPrivate
            }
            callback(null, obj);
        } else {

            let obj = {
                isPrivate :false
            }
            callback(null, obj);
        }
    });
}
exports.getProfileDetailsOfOtherUser = getProfileDetailsOfOtherUser;


var updateProfileStatus = function(data, callback) {

    var criteria = {
        instaChainUserId: data.instaChainUserId,
        userName: data.userName,
    };
    var setQuery = {
        '$set': {
            isPrivate: data.payload.isPrivate
        }
    };
    var options = {
        new: true
    };

    Service.makeModule.users.edit(criteria, setQuery, options, function(err, result) {

        if (err) {
            return callback(err);
        } else {
            callback(null, result);
        }
    });
}
exports.updateProfileStatus = updateProfileStatus;