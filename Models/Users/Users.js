mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');

var DataTable = require('mongoose-datatable');

DataTable.configure({ debug: true, verbose: true });
mongoose.plugin(DataTable.init);

var lastMod = require('../lastModPlugin');

autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

var Users = new Schema({
    userName: { type: String, trim: true },
    isPrivate: { type: Boolean, default: false },
    instaChainUserId: { type: String, trim: true },
}, {
    timestamps: true
});



module.exports = mongoose.model('Users', Users);