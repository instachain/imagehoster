var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');

var payload = {
       maxBytes: 20000000,
    parse: true,
    output: 'file',
    allow: 'multipart/form-data',
    timeout: false
}

var schema = {
    put: {

    },
    post: {

    }
}


function UsersRoute(controller, requestSchema, mainKey, subKey, payload) {

    // binding this controller with the controller in the p arent module i.e. routesModule
    routesModule.call(this, controller, requestSchema, mainKey, subKey, payload);
}

UsersRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


UsersRoute.prototype.getParentRoutes = UsersRoute.prototype.getRoutes;
//UsersRoute.prototype.overridedParantFunction = UsersRoute.prototype.ParantFunction;


UsersRoute.prototype.getUserProfile = function(request, reply) {

    var data = {};

    data.payload = request.query;
    return commonRoutes.handleControllerResponseWithoutAuthPromise(this.controller, this.controller.getUser, data);
}


UsersRoute.prototype.updateProfileStatus = function(request, reply) {

    var data = {};

    data.payload = request.payload;
    return commonRoutes.handleControllerResponseWithoutAuthPromise(this.controller, this.controller.updateUser, data);
}

UsersRoute.prototype.getOtherUserProfile = function(request, reply) {

    var data = {};

    data.payload = request.query;
    return commonRoutes.handleControllerResponseWithoutAuthPromise(this.controller, this.controller.getOtherUserProfile, data);
}

UsersRoute.prototype.getRoutes = function(request, reply) {

    var seperator = '';
    if (this.apiName) {
        seperator = '/'
    }

    var newRoutes = [{
        method: 'GET',
        path: '/v1/user',
        handler: this.getUserProfile.bind(this),
        config: {
            validate: {
                query: {
                    userName: Joi.string().required(),
                    postingKey: Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'get user profile',
            tags: ['api', 'user'],
            plugins: commonRoutes.routesPlugin
        }
    },{
        method: 'GET',
        path: '/v1/profile',
        handler: this.getOtherUserProfile.bind(this),
        config: {
            validate: {
                query: {
                    otherUserName: Joi.string().required(),
                    userName: Joi.string().required(),
                    postingKey: Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'get other user profile',
            tags: ['api', 'user'],
            plugins: commonRoutes.routesPlugin
        }
    },{
        method: 'POST',
        path: '/v1/user',
        handler: this.updateProfileStatus.bind(this),
        config: {
            payload: payload,
            validate: {
                payload: {
                    userName: Joi.string().required(),
                    postingKey: Joi.string().required(),
                    isPrivate: Joi.boolean().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'update user profile',
            tags: ['api', 'user'],
            plugins: commonRoutes.routesPlugin
        }
    }];

    return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
    'users': new UsersRoute(Controller.makeModule.users, schema, 'admins', 'users')
};