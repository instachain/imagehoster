var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');

var async = require('async');

var UploadManager = require('../../Lib/UploadManager');
var TokenManager = require('../../Lib/TokenManager');
var CodeGenerator = require('../../Lib/CodeGenerator');

var helper = require('./userControllerHelper');

var config = require('config');

function startSection(sectionName) {

    console.log('=====================' + sectionName + '===================')
}


function UsersController(service) {


    //console.log('============================================UsersController controller initialised')
    ControllerModule.call(this, service);
}

UsersController.prototype = Object.create(ControllerModule.prototype)


UsersController.prototype.updateUser = function updateUser(data, callback) {

    var waterfallArray = [];

    waterfallArray.push(helper.getUserProfileDataFromBlockChain.bind(null, data));
    waterfallArray.push(helper.verifyBlockchainUser);
    waterfallArray.push(helper.getUserIdforBlockChainUserName);
    waterfallArray.push(helper.updateProfileStatus);

    controllerHelper.handleWaterFallFunctions(waterfallArray, callback);
}


UsersController.prototype.getUser = function getUser(data, callback) {

    var waterfallArray = [];

    waterfallArray.push(helper.getUserProfileDataFromBlockChain.bind(null, data));
    waterfallArray.push(helper.verifyBlockchainUser);
    waterfallArray.push(helper.getUserIdforBlockChainUserName);

    controllerHelper.handleWaterFallFunctions(waterfallArray, function(err,result){

        if(err){
            return callback(err);
        }else{
            return callback(null,result.user);
        }


    });
}

UsersController.prototype.getOtherUserProfile = function getUser(data, callback) {

    var waterfallArray = [];

    waterfallArray.push(helper.getUserProfileDataFromBlockChain.bind(null, data));
    waterfallArray.push(helper.verifyBlockchainUser);
    waterfallArray.push(helper.getProfileDetailsOfOtherUser);

    controllerHelper.handleWaterFallFunctions(waterfallArray, function(err,result){

        if(err){
            return callback(err);
        }else{
            return callback(null,result);
        }
    });
}



module.exports = {
    'users': new UsersController(Service.makeModule.users)
};;