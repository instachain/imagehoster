var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
// {type: [Number , String , Date] , trim: true, required: true, unique: true , default: Date.now}
var lastMod = require('../lastModPlugin');

var EditHistories = new Schema({
	dataObj: {type: Object}
},{
    timestamps : true
});


module.exports = mongoose.model('EditHistories', EditHistories);