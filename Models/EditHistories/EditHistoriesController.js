var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');


function startSection(sectionName) {

   console.log('=====================' + sectionName + '===================')
}


function EditHistoriesController(service) {

   ControllerModule.call(this, service);
}

EditHistoriesController.prototype = Object.create(ControllerModule.prototype)


module.exports = {
   'editHistories': new EditHistoriesController(Service.makeModule.editHistories)
};;