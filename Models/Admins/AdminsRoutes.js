var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');

var payload = {
    maxBytes: 20000000,
    parse: true,
    output: 'file',
}

var schema = {
    put: {

    },
    post: {

    }
}


function AdminsRoute(controller, requestSchema, mainKey, subKey, payload) {



    // binding this controller with the controller in the p arent module i.e. routesModule
    routesModule.call(this, controller, requestSchema, mainKey, subKey, payload);
}

AdminsRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


AdminsRoute.prototype.getParentRoutes = AdminsRoute.prototype.getRoutes;
//AdminsRoute.prototype.overridedParantFunction = AdminsRoute.prototype.ParantFunction;


AdminsRoute.prototype.adminLogin = function(request, reply) {

    var data = {};

    data.payload = request.payload;

    data.payload.ipAddress = request.info.remoteAddress || null;

    // this.controller.adminLogin(data, commonRoutes.handleControllerResponsePost.bind({
    //     reply: reply,
    //     request: request
    // }));


    return commonRoutes.handleControllerResponseWithoutAuthPromise.call({
        reply: reply,
        request: request
    }, this.controller, this.controller.adminLogin, data);

}

AdminsRoute.prototype.adminLogout = function(request, reply) {

    var data = {};

    var token = request.auth.credentials.token;
    var adminData = request.auth.credentials.adminData;

    if (!token) {
        reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN));
    } else if (adminData && adminData.role != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN) {
        reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
    }

    data.payload = {
        token: token
    }

    data.adminData = adminData;

    // this.controller.adminLogout(data, commonRoutes.handleControllerResponse.bind({
    //     reply: reply,
    //     request: request
    // }));

    return commonRoutes.handleControllerResponsePromise.call({
        reply: reply,
        request: request
    }, this.controller, this.controller.adminLogout, data);

}

AdminsRoute.prototype.getAdminConstants = function(request, reply) {

    var adminData = request.auth && request.auth.credentials && request.auth.credentials.adminData;

    if (adminData && adminData.id) {
        reply(UniversalFunctions.sendSuccess(null, UniversalFunctions.CONFIG.DATABASE))
    } else {
        reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
    }

}

AdminsRoute.prototype.changePassword = function(request, reply) {

    var adminData = request.auth && request.auth.credentials && request.auth.credentials.adminData || null;

    if (!adminData || adminData._id != request.params._id)
        return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));

    var data = {
        payload: request.payload,
        adminData: adminData
    }

    data.payload._id = request.params._id;

    // this.controller.changePassword(data, commonRoutes.handleControllerResponse.bind({
    //     reply: reply,
    //     request: request
    // }));


    return commonRoutes.handleControllerResponsePromise.call({
        reply: reply,
        request: request
    }, this.controller, this.controller.changePassword, data);

}

AdminsRoute.prototype.getRoutes = function(request, reply) {

    var seperator = '';
    if (this.apiName) {
        seperator = '/'
    }

    // var newRoutes = [

    //  //   You can write new routes here


    //     {
    //         method: 'POST',
    //         path: '/v1/' + this.apiName + '/login',
    //         handler: this.adminLogin.bind(this),
    //         config: {
    //             validate: {
    //                 payload: {
    //                     email: Joi.string().email().required(),
    //                     password: Joi.string().required()
    //                 },
    //                 failAction: UniversalFunctions.failActionFunction
    //             },
    //             description: 'Login for Super Admin',
    //             tags: ['api',"admin", this.moduleName],
    //             plugins: commonRoutes.routesPlugin
    //         }
    //     }, {
    //         method: 'PUT',
    //         path: '/v1/' + this.apiName + '/logout',
    //         handler: this.adminLogout.bind(this),
    //         config: {
    //             auth: 'AdminAuth',
    //             validate: {
    //                 headers: UniversalFunctions.authorizationHeaderObj,
    //                 failAction: UniversalFunctions.failActionFunction
    //             },
    //             description: 'logout for Super Admin',
    //             tags: ['api', "admin",this.moduleName],
    //             plugins: commonRoutes.routesPlugin
    //         }
    //     }, {
    //         method: 'GET',
    //         path: '/v1/' + this.apiName + '/constants',
    //         handler: this.getAdminConstants.bind(this),
    //         config: {
    //             auth: 'AdminAuth',
    //             validate: {
    //                 headers: UniversalFunctions.authorizationHeaderObj,
    //                 failAction: UniversalFunctions.failActionFunction
    //             },
    //             description: 'get admin constants',
    //             tags: ['api',"admin", this.moduleName],
    //             plugins: commonRoutes.routesPlugin
    //         }
    //     }, {
    //         method: 'PUT',
    //         path: '/v1/' + this.apiName + '/changePassword/{_id}',
    //         handler: this.changePassword.bind(this),
    //         config: {
    //             auth: 'AdminAuth',
    //             validate: {
    //                 headers: UniversalFunctions.authorizationHeaderObj,
    //                 payload: {
    //                     password: Joi.string().required().min(5).trim(),
    //                     newPassword: Joi.string().required().min(5).trim()
    //                 },
    //                 params: {
    //                     _id: Joi.string().required()
    //                 },
    //                 failAction: UniversalFunctions.failActionFunction
    //             },
    //             description: 'change passsword for Super Admin',
    //             tags: ['api',"admin", this.moduleName],
    //             plugins: commonRoutes.routesPlugin
    //         }
    //     }



    // ]

    var newRoutes = [];



    return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
    'admins': new AdminsRoute(Controller.makeModule.admins, schema, 'admins', 'admins')
};