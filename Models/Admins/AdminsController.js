var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');

var async = require('async');

var UploadManager = require('../../Lib/UploadManager');
var TokenManager = require('../../Lib/TokenManager');

function startSection(sectionName) {

    console.log('=====================' + sectionName + '===================')
}


function AdminsController(service) {


    //console.log('============================================AdminsController controller initialised')
    ControllerModule.call(this, service);
}

AdminsController.prototype = Object.create(ControllerModule.prototype)

AdminsController.prototype.adminLogin = function adminLogin(data, callback) {

    var tokenToSend = null;
    var responseToSend = {};
    var tokenData = null;
    var adminData = null;
    var id = null;

    async.series([
        function(cb) {
            var getCriteria = {
                email: data.payload.email,
                password: UniversalFunctions.CryptData(data.payload.password)
            };

            Service.makeModule.admins.view(getCriteria, {}, {}, function(err, result) {
                if (err) {
                    cb({ errorMessage: 'DB Error: ' + err })
                } else {
                    if (result && result.length > 0 && result[0].email) {
                        tokenData = {
                            id: result[0]._id,
                            username: result[0].username,
                            type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
                        };

                        adminData = result[0];

                        id = result[0]._id;


                        cb()
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                    }
                }
            });
        },
        function(cb) {
            var setCriteria = {
                email: data.payload.email
            };
            var setQuery = {
                $push: {
                    loginAttempts: {
                        validAttempt: (tokenData != null),
                        ipAddress: data.payload.ipAddress
                    }
                }
            };
            Service.makeModule.admins.edit(setCriteria, setQuery, { new: true }, function(err, result) {
                cb(err, result);
            });
        },
        function(cb) {
            if (tokenData && tokenData.id) {

                var TokenManager = require('../../Lib/TokenManager');


                TokenManager.setToken(tokenData, function(err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        console.log(output)
                        tokenToSend = output && output.accessToken || null;

                        cb();
                    }
                });

            } else {
                cb()
            }
        }
    ], function(err, result) {
        console.log('sending response')

        if (err) {
            callback(err);
        } else {
            responseToSend = { token: tokenToSend, ipAddress: data.payload.ipAddress, _id: id, isSuperDuperAdmin: adminData.isSuperDuperAdmin };
            callback(null, responseToSend)
        }

    });

}

AdminsController.prototype.adminLogout = function adminLogout(data, callback) {
    TokenManager.expireToken(data.payload.token, function(err, result) {
        console.log("expiring token ======================>>>>>>>> adminLogout")
        console.log(err, result)

        if (err) {
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED)

        } else {
            callback(null, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT);
        }
    })
}

AdminsController.prototype.changePassword = function changePassword(data, callback) {
    var userFound = null;
    if (!data.payload.password || !data.payload.newPassword || !data.adminData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function(cb) {
                var criteria = {
                    _id: data.adminData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.makeModule.admins.view(criteria, projection, options, function(err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length > 0 && result[0]._id) {
                            userFound = result[0];
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function(cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.password)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                } else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                } else {
                    cb();
                }
            },
            function(cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin: false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.makeModule.admins.edit(criteria, setQuery, options, cb);
            }

        ], function(err, result) {
            callback(err, null);
        })
    }
}

module.exports = {
    'admins': new AdminsController(Service.makeModule.admins)
};;
