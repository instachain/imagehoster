var UniversalFunctions = require('../../Utils/UniversalFunctions');
var Service = require('../../Services');
var controllerHelper = require('../../Controllers/commonControllerFunctions');


function ResetPasswordModule(model, resetPasswordKey, webUrl, emailTemplate, isCreatePassword) {

    this.passwordResetTokenKey = resetPasswordKey || "passwordResetToken"
    this.model = model;
    this.webUrl = webUrl;
    this.isCreatePassword = isCreatePassword || false;


}


ResetPasswordModule.prototype.createPasswordResetToken = function(data,callback){
	    var waterfallArray = [];
	    waterfallArray.push(this.createPasswordResetTokenInDb.bind(this, data));
	    waterfallArray.push(this.sendEmailToUser.bind(this));

	    controllerHelper.handleWaterFallFunctions(waterfallArray, callback)
}


ResetPasswordModule.prototype.createPasswordResetTokenInDb = function(data, callback) {

    var _this = this;

    if (!data.email) {
        return callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    }

    var generatedString = UniversalFunctions.generateRandomString();

    var criteria = {
        email: data.email
    };
    var setQuery = {
        [_this.passwordResetTokenKey]: UniversalFunctions.CryptData(generatedString)
    };
    Service.makeModule[_this.model].edit(criteria, setQuery, {
        new: true
    }, function(err, userData) {

        if (err) {
            callback(err)
        } else {
            if (!userData || userData.length == 0) {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
            } else {
                data.userData = userData
                callback(null, data)
            }
        }
    })

}


ResetPasswordModule.prototype.sendEmailToUser = function(data, callback) {

    var _this = this;

    if (data.userData) {
        var variableDetails = {
            user_name: data.userData.name,
            password_reset_token: data.userData.passwordResetToken,
            password_reset_link: _this.webUrl + '?token=' + data.userData[_this.passwordResetTokenKey] + '&email=' + data.email + "&createPassword=" + _this.isCreatePassword //TODO change this to proper html page link
        };

        // NotificationManager.sendEmailToUser(_this.emailTemplate, variableDetails, data.userData.email, callback)
    } else {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }

}



ResetPasswordModule.prototype.resetPassword = function(data, callback) {
    var _this = this;

    if (!data || !data.email || !data[passwordResetTokenKey] || !data.newPassword) {
        return callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    }


    var criteria = {
        email: payloadData.email,
        passwordResetToken: passwordResetToken
    };

    var setQuery = {
        password: UniversalFunctions.CryptData(payloadData.newPassword),
        $unset: {
            passwordResetToken: 1
        }

    }
    Service.makeModule[this.model].edit(criteria, {}, {
        lean: true
    }, function(err, userData) {
        if (err) {
            callback(err)
        } else {
            if (!userData || userData.length == 0) {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RESET_PASSWORD_TOKEN);
            } else {
                
                callback(null)
            }
        }
    })
}





function createResetPasswordPlugin(model, resetPasswordKey, webUrl, emailTemplate, isCreatePassword){

return new ResetPasswordModule(model, resetPasswordKey, webUrl, emailTemplate, isCreatePassword);
}

exports.resetPasswordPlugin = resetPasswordPlugin;