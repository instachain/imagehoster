var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');


var Notifications = new Schema({

  associatedUser: {
    kind: {type: String},
    referId: { type: Schema.ObjectId, refPath: 'associatedUser.kind' }
  },
  referal : {
    kind: {type: String},
    referId: { type: Schema.ObjectId, refPath: 'referal.kind' }
  },
  notificationTime: {
    type: Date,
    required: true
  },
  // notificationRelatedTo: {
  //   type: String,
  //   trim: true,
  //   index: true,
  //   default: null,
  //   sparse: true,
  //   enum: [
  //           Config.APP_CONSTANTS.DATABASE.WEB_NOTIFICATION_RELATION.BOOKING_RELATED,
  //           Config.APP_CONSTANTS.DATABASE.WEB_NOTIFICATION_RELATION.MESSAGE_RELATED
  //       ]
  // },
  notificationType: {
    type: String,
    trim: true,
    index: true,
    default: 'NA',
    sparse: true,
    enum: [
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.APPOINTMENT,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.ARTICLE,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.CHAT,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.INSTRUCTION,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.CONNECT_REQ,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.ADDED_TO_TEAM,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.ADMISSION,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.ORG_JOIN,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.APPOINTMENT_REFERRAL,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.HANDOVER,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.RECORD_SHARING,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.PATIENT_TRANSFER,
            Config.APP_CONSTANTS.NOTIFICATIONS.TYPE.OPERATION,
            'NA'
        ]
  },
  viewed: {
    type: Boolean,
    default: false
  },
  // performedBy: {
  //   kind: {type: String},
  //   referId: { type: Schema.ObjectId, refPath: 'referal.kind' }
  // },
  description: {
    type: String,
    trim: true,
    index: true,
    default: null,
    sparse: true
  },
  mainText: {
    type: String,
    trim: true,
    index: true,
    default: '',
    sparse: true
  },
  notifyLaterTime: {
    type: Date,
    required: true
  },
  url:{
    type: String,
    trim: true,
    index: true,
    default: null,
    sparse: true
  },
  isUrlInside : {
    type: Boolean,
    default: false
  }
},{
    timestamps : true
});

module.exports = mongoose.model('Notifications', Notifications);
