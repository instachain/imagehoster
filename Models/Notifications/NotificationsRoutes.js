var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');

var payload = {
    maxBytes: 20000000,
    parse: true,
    output: 'file',
}

var schema = {
    put: {
        
    },
    post: {
       
    }
}


function NotificationsRoute(controller, requestSchema, mainKey, subKey , payload) {



    // binding this controller with the controller in the p arent module i.e. routesModule
    routesModule.call(this, controller, requestSchema, mainKey, subKey , payload);
}

NotificationsRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


NotificationsRoute.prototype.getParentRoutes = NotificationsRoute.prototype.getRoutes;
//NotificationsRoute.prototype.overridedParantFunction = NotificationsRoute.prototype.ParantFunction;


NotificationsRoute.prototype.newFunction = function(request, reply){


     this.controller.anyController(request.params.id, commonRoutes.handleControllerResponseWithoutAuth.bind({
        reply: reply,
        request: request
    }));

}

NotificationsRoute.prototype.getRoutes = function(request, reply) {

    var seperator = '';
    if (this.apiName) {
        seperator = '/'
    }

    var newRoutes =[ 

    // You can write new routes here
    // 
    
    // {
    //     method: 'GET',
    //     path: '/api/' + this.apiName ,
    //     handler: this.newFunction.bind(this),
    //     config: {
    //         validate: {
    //             query: {
    //             }

    //         },
    //         description: 'get a module by its id',
    //         tags: ['api', this.moduleName],
    //         plugins: commonRoutes.routesPlugin
    //     }
    // }



    ]


    return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
    'notifications': new NotificationsRoute(Controller.makeModule.notifications, schema, 'admins', 'notifications')
};