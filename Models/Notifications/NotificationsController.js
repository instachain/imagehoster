var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');


function startSection(sectionName) {

   console.log('=====================' + sectionName + '===================')
}


function NotificationsController(service) {

 
   //console.log('============================================NotificationsController controller initialised')
   ControllerModule.call(this, service);
}

NotificationsController.prototype = Object.create(ControllerModule.prototype)


module.exports = {
   'notifications': new NotificationsController(Service.makeModule.notifications)
};;