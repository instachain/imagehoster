var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
// {type: [Number , String , Date] , trim: true, required: true, unique: true , default: Date.now}
var lastMod = require('../lastModPlugin');

var AdminConstants = new Schema({
	
	formulations : [{type:String}],
	strengths : [{type:String}],
	quantities : [{type:String}],
	routes : [{type:String}],
	adminstrativeSites : [{type:String}],
	frequencies : [{type:String}]

},{
	timestamps : true
});


module.exports = mongoose.model('AdminConstants', AdminConstants);
