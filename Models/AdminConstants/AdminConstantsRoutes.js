var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');

var payload = {
maxBytes: 20000000,
parse: true,
output: 'file',
}

var schema = {

	put: {
		_id: Joi.string().required(),
		formulations:Joi.array().items(Joi.string()),
		strengths:Joi.array().items(Joi.string()),
		quantities:Joi.array().items(Joi.string()),
		routes:Joi.array().items(Joi.string()),
		adminstrativeSites:Joi.array().items(Joi.string()),
		frequencies:Joi.array().items(Joi.string()),

	}
}


function AdminConstantsRoute(controller, requestSchema, mainKey, subKey , payload) {



// binding this controller with the controller in the p arent module i.e. routesModule
routesModule.call(this, controller, requestSchema, mainKey, subKey , payload);
}

AdminConstantsRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


AdminConstantsRoute.prototype.getParentRoutes = AdminConstantsRoute.prototype.getRoutes;
//AdminConstantsRoute.prototype.overridedParantFunction = AdminConstantsRoute.prototype.ParantFunction;


AdminConstantsRoute.prototype.newFunction = function(request, reply){


this.controller.anyController(request.params.id, commonRoutes.handleControllerResponseWithoutAuth.bind({
reply: reply,
request: request
}));

}

AdminConstantsRoute.prototype.getRoutes = function(request, reply) {

var seperator = '';
if (this.apiName) {
seperator = '/'
}

var newRoutes =[ 

// You can write new routes here
// 

// {
//     method: 'GET',
//     path: '/v1/' + this.apiName ,
//     handler: this.newFunction.bind(this),
//     config: {
//         validate: {
//             query: {
//             }

//         },
//         description: 'get a module by its id',
//         tags: ['api', this.moduleName],
//         plugins: commonRoutes.routesPlugin
//     }
// }



]


return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
'adminConstants': new AdminConstantsRoute(Controller.makeModule.adminConstants, schema, 'admins', 'adminConstants')
};