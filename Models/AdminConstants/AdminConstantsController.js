var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');


function startSection(sectionName) {

console.log('=====================' + sectionName + '===================')
}


function AdminConstantsController(service) {

 
//console.log('============================================AdminConstantsController controller initialised')
ControllerModule.call(this, service);
}

AdminConstantsController.prototype = Object.create(ControllerModule.prototype)


module.exports = {
'adminConstants': new AdminConstantsController(Service.makeModule.adminConstants)
};