var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');

var apiHistory = new Schema({

    apiType: {type: String, default: ""},
    path : {type: String, default: ""},
    ip : {type: String, default: ""},
    requestData : { type : Object},
    userId: {
      type: Schema.ObjectId,
      ref: 'Users'
    },
    userType: {type: String},
    userId: { type: Schema.ObjectId, refPath: 'userType' },
    loc: { type: [Number], index: '2dsphere', default: [0, 0] },
    address : {type: String, default: ""},
    hostname:   {type: String, default: ""},
    referrer :   {type: String, default: ""},
    host :   {type: String, default: ""}, 
    userAgent :  {type: String, default: ""},
    responseData : { type : Object}

},
{
    timestamps: true
});


module.exports = mongoose.model('ApiHistories', apiHistory);