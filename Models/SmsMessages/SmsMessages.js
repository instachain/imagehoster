var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
// {type: [Number , String , Date] , trim: true, required: true, unique: true , default: Date.now}
var lastMod = require('../lastModPlugin');

var SmsMessages = new Schema({
	title: {type: String, trim: true, required: true, unique: true},
    body: {type: String, trim: true, required: true},
    handlebarVars: [{type: String, trim: true}],
    lastUpdated: {type: Date, default: Date.now, required: true},
},{
timestamps : true
});


module.exports = mongoose.model('SmsMessages', SmsMessages);
