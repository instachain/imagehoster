var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');

var payload = {
maxBytes: 20000000,
parse: true,
output: 'file',
}

var schema = {
	put: {
		_id: Joi.string().required(),
		title: Joi.string(),
		body: Joi.string(),
		handlebarVars: Joi.array()
	},
	post: {
		title: Joi.string().required(),
		body: Joi.string().required(),
		handlebarVars: Joi.array()
	}
}


function SmsMessagesRoute(controller, requestSchema, mainKey, subKey , payload) {



// binding this controller with the controller in the p arent module i.e. routesModule
routesModule.call(this, controller, requestSchema, mainKey, subKey , payload);
}

SmsMessagesRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


SmsMessagesRoute.prototype.getParentRoutes = SmsMessagesRoute.prototype.getRoutes;
//SmsMessagesRoute.prototype.overridedParantFunction = SmsMessagesRoute.prototype.ParantFunction;


SmsMessagesRoute.prototype.newFunction = function(request, reply){


this.controller.anyController(request.params.id, commonRoutes.handleControllerResponseWithoutAuth.bind({
reply: reply,
request: request
}));

}

SmsMessagesRoute.prototype.getRoutes = function(request, reply) {

var seperator = '';
if (this.apiName) {
seperator = '/'
}

var newRoutes =[ 

// You can write new routes here
// 

// {
//     method: 'GET',
//     path: '/v1/' + this.apiName ,
//     handler: this.newFunction.bind(this),
//     config: {
//         validate: {
//             query: {
//             }

//         },
//         description: 'get a module by its id',
//         tags: ['api', this.moduleName],
//         plugins: commonRoutes.routesPlugin
//     }
// }



]


return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
'smsMessages': new SmsMessagesRoute(Controller.makeModule.smsMessages, schema, 'admins', 'smsMessages')
};