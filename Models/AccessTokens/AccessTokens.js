var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
// {type: [Number , String , Date] , trim: true, required: true, unique: true , default: Date.now}
var lastMod = require('../lastModPlugin');

var AccessTokens = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'Users'
    },
    accessToken: { type: String, index: true, unique: true, required: true },
    // loginAs: { type: String, required: true }, // parent, vendor
    deviceToken: { type: String },
    deviceId: { type: String },
    deviceType: {
        type: String,
        enum: [
            Config.APP_CONSTANTS.DATABASE.deviceType.Website,
            Config.APP_CONSTANTS.DATABASE.deviceType.IOS,
            Config.APP_CONSTANTS.DATABASE.deviceType.Android
        ]
    }
}, {
    timestamps: true
});


module.exports = mongoose.model('AccessTokens', AccessTokens);
