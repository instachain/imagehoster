var Service = require('../../Services');
var Models = require('../../Models');
var ControllerModule = require('../../Controllers/ControllerModule').ControllerModule;
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var controllerHelper = require('../../Controllers/commonControllerFunctions');
var Config = require('../../constant');
// With ES5
var JiraApi = require('jira-client');
var fs = require('fs');


// Initialize
var jira = new JiraApi({
    protocol: 'https',
    host: 'medrexa.atlassian.net',
    username: 'pankaj',
    password: 'chill123',
    apiVersion: '2',
    strictSSL: false
});

function startSection(sectionName) {

    console.log('=====================' + sectionName + '===================')
}


function FeedbackController(service) {


    //console.log('============================================FeedbackController controller initialised')
    ControllerModule.call(this, service);
}

FeedbackController.prototype = Object.create(ControllerModule.prototype)

// var obj =  [ { self: 'https://medrexa.atlassian.net/rest/api/2/component/10000',
//     id: '10000',
//     name: 'Android App',
//     lead: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=ashu',
//        key: 'ashu',
//        name: 'ashu',
//        avatarUrls: [Object],
//        displayName: 'Ashu',
//        active: true },
//     assigneeType: 'COMPONENT_LEAD',
//     assignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=ashu',
//        key: 'ashu',
//        name: 'ashu',
//        avatarUrls: [Object],
//        displayName: 'Ashu',
//        active: true },
//     realAssigneeType: 'COMPONENT_LEAD',
//     realAssignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=ashu',
//        key: 'ashu',
//        name: 'ashu',
//        avatarUrls: [Object],
//        displayName: 'Ashu',
//        active: true },
//     isAssigneeTypeValid: true,
//     project: 'MHU',
//     projectId: 10001 },
//   { self: 'https://medrexa.atlassian.net/rest/api/2/component/10001',
//     id: '10001',
//     name: 'IOS App',
//     lead: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=rohit',
//        key: 'rohit',
//        name: 'rohit',
//        avatarUrls: [Object],
//        displayName: 'Rohit',
//        active: true },
//     assigneeType: 'COMPONENT_LEAD',
//     assignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=rohit',
//        key: 'rohit',
//        name: 'rohit',
//        avatarUrls: [Object],
//        displayName: 'Rohit',
//        active: true },
//     realAssigneeType: 'COMPONENT_LEAD',
//     realAssignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=rohit',
//        key: 'rohit',
//        name: 'rohit',
//        avatarUrls: [Object],
//        displayName: 'Rohit',
//        active: true },
//     isAssigneeTypeValid: true,
//     project: 'MHU',
//     projectId: 10001 },
//   { self: 'https://medrexa.atlassian.net/rest/api/2/component/10003',
//     id: '10003',
//     name: 'Web Api',
//     lead: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=pankaj',
//        key: 'pankaj',
//        name: 'pankaj',
//        avatarUrls: [Object],
//        displayName: 'Pankaj Jindal',
//        active: true },
//     assigneeType: 'COMPONENT_LEAD',
//     assignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=pankaj',
//        key: 'pankaj',
//        name: 'pankaj',
//        avatarUrls: [Object],
//        displayName: 'Pankaj Jindal',
//        active: true },
//     realAssigneeType: 'COMPONENT_LEAD',
//     realAssignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=pankaj',
//        key: 'pankaj',
//        name: 'pankaj',
//        avatarUrls: [Object],
//        displayName: 'Pankaj Jindal',
//        active: true },
//     isAssigneeTypeValid: true,
//     project: 'MHU',
//     projectId: 10001 },
//   { self: 'https://medrexa.atlassian.net/rest/api/2/component/10002',
//     id: '10002',
//     name: 'Web App',
//     lead: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=govind',
//        key: 'govind',
//        name: 'govind',
//        avatarUrls: [Object],
//        displayName: 'Govind',
//        active: true },
//     assigneeType: 'COMPONENT_LEAD',
//     assignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=govind',
//        key: 'govind',
//        name: 'govind',
//        avatarUrls: [Object],
//        displayName: 'Govind',
//        active: true },
//     realAssigneeType: 'COMPONENT_LEAD',
//     realAssignee: 
//      { self: 'https://medrexa.atlassian.net/rest/api/2/user?username=govind',
//        key: 'govind',
//        name: 'govind',
//        avatarUrls: [Object],
//        displayName: 'Govind',
//        active: true },
//     isAssigneeTypeValid: true,
//     project: 'MHU',
//     projectId: 10001 } ];




// id: '10004',
//    key: 'MA',
//    name: 'Medrexa Admin',


// id: '10003',
//    key: 'MHO',
//    name: 'Medrexa Health Organizations',

//      id: '10002',
//    key: 'MHP',
//    name: 'Medrexa Health Professionals',

//  id: '10001',
//    key: 'MHU',
//    name: 'Medrexa Health Users',

//       id: '10100',
//    key: 'MW',
//    name: 'Medrexa Website',




FeedbackController.prototype.addFeedback = function(data, callback) {


    var waterfallArray = [];

    waterfallArray.push(this.addIssue.bind(this, data));
    waterfallArray.push(this.addAttachmentForIssue.bind(this));
    waterfallArray.push(this.insertFeedback.bind(this));

    controllerHelper.handleWaterFallFunctions(waterfallArray, callback);

    // addIssue();

    // function listProjects() {

    //     jira.listProjects()
    //         .then(function(projects) {
    //             console.log('======================listProjects: =====================', projects);
    //             // console.log('======================listProjects stringify: =====================',JSON.stringify(projects));
    //         })
    //         .catch(function(err) {
    //             console.error('======================listProjects err: =====================', err);
    //         });
    // }

    // function addIssue() {

    //     jira.addNewIssue(obj)
    //         .then(function(issue) {
    //             console.log('======================addNewIssue: =====================', issue);
    //             // console.log('======================listProjects stringify: =====================',JSON.stringify(projects));
    //         })
    //         .catch(function(err) {
    //             console.error('======================addNewIssue err: =====================', err);
    //         });
    // }

    // function listProjectComponents() {

    //     jira.listComponents("10001")
    //         .then(function(components) {
    //             console.log('======================listComponents: =====================', components);
    //             // console.log('======================listProjects stringify: =====================',JSON.stringify(projects));
    //         })
    //         .catch(function(err) {
    //             console.error('======================listComponents err: =====================', err);
    //         });
    // }
    // return callback(null, {});


}

FeedbackController.prototype.insertFeedback = function(data, callback) {

    var objToSave = data.payload;

    this.addService(objToSave, function(err, result) {

        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    });

}

FeedbackController.prototype.addAttachmentForIssue = function(data, callback) {

    var objToSave = data.payload;

    if (objToSave.isFeedback || !objToSave.isMedia || !objToSave.issueId|| !objToSave.file) {
        return callback(null, data);
    } else {
        var path = objToSave.file.path; //will be put into a temp directory

            // fs.readFile(path, function(error, file_buffer) {



        jira.addAttachmentOnIssue(objToSave.issueId, fs.createReadStream(path))
            .then(function(issue) {
                console.log('======================addAttachmentOnIssue: success =====================', issue);
                return callback(null, data);

            })
            .catch(function(err) {
                console.error('======================addAttachmentOnIssue err: =====================', err);
                return callback(null, data);
            });

        // });
    }
}

FeedbackController.prototype.addIssue = function(data, callback) {


    var objToSave = data.payload;
    // data.payload.issueId = "10234";
    // return callback(null, data);

    if (objToSave.isFeedback) {
        return callback(null, data);

    } else {
        var obj = {
            "update": {},
            "fields": {
                "project": {
                    "key": objToSave.project
                },
                // "summary": "IGNORE : Checking through API",
                "summary": objToSave.title,
                "issuetype": {
                    "name": "Bug"
                },
                // "description": "IGNORE",
                "description": objToSave.text,
                "components": [{
                    // "name": "Android App"
                    "name": objToSave.component
                }]
            }
        };

        jira.addNewIssue(obj)
            .then(function(issue) {
                console.log('======================addNewIssue: =====================', issue);
                data.payload.issueId = issue.id;
                return callback(null, data);

                // console.log('======================listProjects stringify: =====================',JSON.stringify(projects));
            })
            .catch(function(err) {
                console.error('======================addNewIssue err: =====================', err);
                return callback(null, data);

            });
    }
}




module.exports = {
    'feedback': new FeedbackController(Service.makeModule.feedback)
};;
