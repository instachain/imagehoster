var routesModule = require('../../Routes/RoutesModule').Routes;
var Controller = require('../../Controllers');
var UniversalFunctions = require('../../Utils/UniversalFunctions');
var commonRoutes = require('../../Routes/commonRoutesThings');
var Joi = require('joi');

var payload = {
    maxBytes: 20000000,
    parse: true,
    output: 'file',
}

var schema = {
    put: {

    },
    post: {

    }
}


function FeedbackRoute(controller, requestSchema, mainKey, subKey, payload) {



    // binding this controller with the controller in the p arent module i.e. routesModule
    routesModule.call(this, controller, requestSchema, mainKey, subKey, payload);
}

FeedbackRoute.prototype = Object.create(routesModule.prototype) // inheritance happening


FeedbackRoute.prototype.getParentRoutes = FeedbackRoute.prototype.getRoutes;
//FeedbackRoute.prototype.overridedParantFunction = FeedbackRoute.prototype.ParantFunction;


FeedbackRoute.prototype.newFunction = function(request, reply) {


    this.controller.anyController(request.params.id, commonRoutes.handleControllerResponseWithoutAuth.bind({
        reply: reply,
        request: request
    }));

}

FeedbackRoute.prototype.getRoutes = function(request, reply) {

    var seperator = '';
    if (this.apiName) {
        seperator = '/'
    }
    var _this = this;

    var newRoutes = [];

    return this.getParentRoutes().concat(newRoutes);
}

module.exports = {
    'feedback': new FeedbackRoute(Controller.makeModule.feedback, schema, 'admins', 'feedback')
};
