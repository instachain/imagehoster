var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../../constant');
// {type: [Number , String , Date] , trim: true, required: true, unique: true , default: Date.now}
var lastMod = require('../lastModPlugin');

var Feedback = new Schema({
	title: { type: String, trim: true, default: '' },
    text: { type: String, trim: true, default: '' },
    component: { type: String, trim: true, default: '' },
    project: { type: String, trim: true, default: '' },
    deviceType: { type: String, trim: true, default: '' },
    isMedia: { type: Boolean, default: false },
    isFeedback: { type: Boolean, default: false },
    createdBy: {
        type: Schema.ObjectId,
        ref: 'Users'
    },
    isDeleted: { type: Boolean, default: false },
},{
    timestamps : true
});


module.exports = mongoose.model('Feedback', Feedback);