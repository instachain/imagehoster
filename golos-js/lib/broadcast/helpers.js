"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _api = require("../api");

var _api2 = _interopRequireDefault(_api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultWeight = 1;

exports = module.exports = function (steemBroadcast) {
  steemBroadcast.addAccountAuth = function (activeWif, username, authorizedUsername) {
    var role = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "posting";
    var cb = arguments[4];

    _api2.default.getAccountsAsync([username]).then(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          userAccount = _ref2[0];

      var updatedAuthority = userAccount[role];
      var authorizedAccounts = updatedAuthority.account_auths.map(function (auth) {
        return auth[0];
      });
      var hasAuthority = authorizedAccounts.indexOf(authorizedUsername) !== -1;

      if (hasAuthority) {
        // user does already exist in authorized list
        return cb(null, null);
      }
      updatedAuthority.account_auths.push([authorizedUsername, defaultWeight]);
      var owner = role === "owner" ? updatedAuthority : undefined;
      var active = role === "active" ? updatedAuthority : undefined;
      var posting = role === "posting" ? updatedAuthority : undefined;
      /** Add authority on user account */
      steemBroadcast.accountUpdate(activeWif, userAccount.name, owner, active, posting, userAccount.memo_key, userAccount.json_metadata, cb);
    });
  };

  steemBroadcast.removeAccountAuth = function (activeWif, username, authorizedUsername) {
    var role = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "posting";
    var cb = arguments[4];

    _api2.default.getAccountsAsync([username]).then(function (_ref3) {
      var _ref4 = _slicedToArray(_ref3, 1),
          userAccount = _ref4[0];

      var updatedAuthority = userAccount[role];
      var totalAuthorizedUser = updatedAuthority.account_auths.length;
      for (var i = 0; i < totalAuthorizedUser; i++) {
        var user = updatedAuthority.account_auths[i];
        if (user[0] === authorizedUsername) {
          updatedAuthority.account_auths.splice(i, 1);
          break;
        }
      }
      // user does not exist in authorized list
      if (totalAuthorizedUser === updatedAuthority.account_auths.length) {
        return cb(null, null);
      }

      var owner = role === "owner" ? updatedAuthority : undefined;
      var active = role === "active" ? updatedAuthority : undefined;
      var posting = role === "posting" ? updatedAuthority : undefined;

      steemBroadcast.accountUpdate(activeWif, userAccount.name, owner, active, posting, userAccount.memo_key, userAccount.json_metadata, cb);
    });
  };
};