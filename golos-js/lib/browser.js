"use strict";

var api = require("./api");
var auth = require("./auth");
var broadcast = require("./broadcast");
var config = require("./config");
var formatter = require("./formatter")(api);
var utils = require("./utils");

var golos = {
  api: api,
  auth: auth,
  broadcast: broadcast,
  config: config,
  formatter: formatter,
  utils: utils
};

if (typeof window !== "undefined") {
  window.golos = golos;
}

if (typeof global !== "undefined") {
  global.golos = golos;
}

exports = module.exports = golos;