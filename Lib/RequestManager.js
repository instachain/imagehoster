const rp = require('request-promise');
const request = require('request');
const config = require('config');

function postApi(token, callback) {

    const options = {
        method: 'GET',
        uri: 'http://85.25.195.93:8010/v1/admins/isAuthenticated',
        headers: {
            'Authorization': 'bearer '+token
        },
        // resolveWithFullResponse: true,
        json: true // Automatically parses the JSON string in the response
    };


    rp(options)
        .then(function(response) {
            return callback(null, response.data);
        })
        .catch(function(err) {

            console.log('************ postApi err *************** : ', err);

            return callback(err);
        });
}

function getApi(obj, token, url, callback) {

        console.log('************ getApi *************** : ', obj,url);


    const options = {
        method: 'GET',
        uri: url,
        qs: obj,
        headers: {
            'Authorization': token
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function(response) {
            return callback(null, response);
        })
        .catch(function(err) {
            console.log('getApi err : ', err);
            return callback(err);
        });
}

function generateHeaderToken(data, callback) {

    const options = {
        method: 'POST',
        uri: config.get('apiBaseUrl') + 'connect/token',
        form: {
            // scope:' readAccess',
            "grant_type": 'password',
            "username": 'pankaj884@gmail.com',
            "password": 'pankaj884',
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function(response) {
            data.token = response.token_type + ' ' + response.access_token;
            return callback(null, data);
        })
        .catch(function(err) {
            console.log('generateHeaderToken err : ', err);
            return callback(err);
        });

}

module.exports = {
    postApi: postApi,
    getApi: getApi,
    generateHeaderToken: generateHeaderToken,
};