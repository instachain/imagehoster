'use strict';

var async = require('async');
var _ = require('underscore');
var UniversalFunctions = require('../Utils/UniversalFunctions');

var Services = require('../Services');

var generateRandomNumber = function(minNum, maxNum) {

    var randm = Math.random();

    return  randm*maxNum < minNum ?  Math.floor(minNum + randm*maxNum) : Math.floor(randm*maxNum)
}
exports.generateRandomNumber = generateRandomNumber;


function randomString() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 10; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

exports.randomString = randomString;