var Config = require('../constant');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var Path = require('path');
var knox = require('knox');
var CodeGenerator = require('./CodeGenerator');
var fsExtra = require('fs-extra');
var config = require('config');
var s3BucketCredentials = config.get('s3Bucket');
var cfsign = require('aws-cloudfront-sign');
var fs = require('fs');
var mime = require('mime');
var AWS = require('aws-sdk');

function getSignedUrl(url, seconds, callback) {

    if (url) {
        var options = {
            keypairId: s3BucketCredentials.keyPairId,
            privateKeyString: s3BucketCredentials.privateKey,
            expireTime: new Date().getTime() + 1000 * (seconds) // 30 secs
        };
        var signedUrl = cfsign.getSignedUrl(url, options);
        return callback(null, signedUrl);

    } else {
        return callback();
    }
}


function uploadFileToS3Bucket(file, folder, callback) {


    var filename = CodeGenerator.randomString() + file.filename; // actual filename of file
    var path = file.path; //will be put into a temp directory
    var mimeType = file.type;

    if (!mimeType) {
        mimeType = file.headers['content-type'];
    }

    fs.readFile(path, function(error, file_buffer) {

        if (error) {
            console.log(" uploadFileToS3Bucket ERROR 1 : ", error);
            return callback(error);
        } else {

            var s3bucket = new AWS.S3({
                accessKeyId: s3BucketCredentials.accessKeyId,
                secretAccessKey: s3BucketCredentials.secretAccessKey,
                signatureVersion: 'v4',
                region: s3BucketCredentials.region
            });

            var params = {
                Bucket: s3BucketCredentials.name,
                Key: folder + '/' + filename,
                Body: file_buffer,
                ContentType: mimeType
            };

            s3bucket.putObject(params, function(err, data) {

                fs.unlink(path, function(err) {
                    if (err) console.log(err);
                });

                if (err) {
                    console.log(" uploadFileToS3Bucket ERROR 2 : ", err);
                    return callback(err);
                } else {
                    return callback(null, filename);
                }
            });
        }
    });
};



function uploadImageFileToS3BucketWithThumbnail1(file, folder, callback) {

    var dataToUpload = [];
    var mimeType = file.type;

    if (!mimeType) {
        mimeType = file.headers['content-type'];
    }

    var filename = CodeGenerator.randomString() + file.filename;
    var thumbName = 'thumb_' + filename;


    var profilePicURL = {
        original: filename,
        thumbnail: thumbName
    };

    var originalPath = file.path;

    // var thumbnailPath = 'uploads/' + thumbName;

    var thumbnailPath = Path.join(Path.resolve("."), "uploads", profilePicURL.thumbnail);

    console.log('**************** originalPath ****************', thumbnailPath)


    createThumbnailImage(originalPath, thumbnailPath, file.size, function(err, filePath) {
        if (err) {
            callback(err);
        } else {
            dataToUpload.push({
                originalPath: originalPath,
                nameToSave: profilePicURL.original,
                mimeType: mimeType
            });

            dataToUpload.push({
                originalPath: thumbnailPath,
                nameToSave: profilePicURL.thumbnail,
                mimeType: mimeType
            });

            ImageParallelUploadToS3(dataToUpload, folder, function(err, result) {

                if (err) {
                    return callback(err);
                } else {
                    callback(null, profilePicURL);
                }
            });
        }
    })
};

function uploadImageFileToS3BucketWithThumbnail(fileData, userId, callbackParent) {

    var profilePicURL = {
        original: null,
        thumbnail: null
    };

    var originalPath = null;
    var thumbnailPath = null;
    var dataToUpload = [];
    var mimeType = fileData.type;

    if (!mimeType) {
        mimeType = fileData.headers['content-type'];
    }

    async.series([
        function(cb) {
            //Validate fileData && userId
            if (!fileData || !fileData.filename) {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            } else {
                // TODO Validate file extensions
                cb();
            }
        },
        function(cb) {
            //Set File Names
            profilePicURL.original = UniversalFunctions.getFileNameWithUserId(false, fileData.filename, userId);
            profilePicURL.thumbnail = UniversalFunctions.getFileNameWithUserId(true, fileData.filename, userId);


            profilePicURL.original = profilePicURL.original.replace(/\s/g, '');
            profilePicURL.thumbnail = profilePicURL.thumbnail.replace(/\s/g, '');


            cb();
        },
        function(cb) {
            //Save File
            var path = Path.join(Path.resolve("."), "uploads", profilePicURL.original);


            CodeGenerator.randomString()
            saveFile(fileData.path, path, function(err, data) {
                cb(err, data)
            })
        },
        function(cb) {
            //Create Thumbnail
            originalPath = Path.join(Path.resolve("."), "uploads", profilePicURL.original);
            thumbnailPath = Path.join(Path.resolve("."), "uploads", profilePicURL.thumbnail);

            console.log('*********** profilePicURL *************', profilePicURL);


            createThumbnailImage(originalPath, thumbnailPath, fileData.size, function(err, data) {
                if (err) {
                    cb(err);
                    console.log('*********** thumbnail error *************', err);
                    // winston.log('error', 'createThumbnailImage', { err, err });
                } else {
                    dataToUpload.push({
                        originalPath: originalPath,
                        nameToSave: profilePicURL.original,
                        mimeType: mimeType
                    });
                    dataToUpload.push({
                        originalPath: data,
                        nameToSave: profilePicURL.thumbnail,
                        mimeType: mimeType
                    });
                    cb(err, data)
                }
            })
        },
        function(cb) {
            //Upload both images on S3
            ImageParallelUploadToS3(dataToUpload, userId, cb);
        }
    ], function(err, result) {
        callbackParent(err, profilePicURL);
    });
};


function ImageParallelUploadToS3(dataToUpload, folder, callback) {

    async.parallel([
            function(cb) {
                upload(dataToUpload[0].originalPath, dataToUpload[0].nameToSave, dataToUpload[0].mimeType, cb);
            },
            function(cb) {
                upload(dataToUpload[1].originalPath, dataToUpload[1].nameToSave, dataToUpload[1].mimeType, cb);
            }
        ],
        function(err, result2) {
            return callback(err, result2);
        });

    function upload(path, filename, mimeType, cb) {

        fs.readFile(path, function(error, file_buffer) {

            if (error) {
                console.log(" uploadFileToS3Bucket ERROR 1 : ", error);
                return cb(error);
            } else {

                var s3bucket = new AWS.S3({
                    accessKeyId: s3BucketCredentials.accessKeyId,
                    secretAccessKey: s3BucketCredentials.secretAccessKey,
                    signatureVersion: 'v4',
                    region: s3BucketCredentials.region
                });

                var params = {
                    Bucket: s3BucketCredentials.name,
                    Key: folder + '/' + filename,
                    Body: file_buffer,
                    ContentType: mimeType
                };

                s3bucket.putObject(params, function(err, data) {

                    fs.unlink(path, function(err) {
                        if (err) console.log(err);
                    });

                    if (err) {
                        console.log(" uploadFileToS3Bucket ERROR 2 : ", err);
                        return cb(err);
                    } else {
                        return cb(null, filename);
                    }
                });
            }
        });
    }
}


/*
 Save File on the disk
 */
function saveFile(fileData, path, callback) {
    fsExtra.copy(fileData, path, callback);
}

function deleteFile(path) {
    fsExtra.remove(path, function(err) {
        console.log('error deleting file>>', err)
    });
}

/*
 Create thumbnail image using graphics magick
 */
function createThumbnailImage(originalPath, thumbnailPath, size, callback) {

    var imageSize = {
        height: config.get('THUMB_HEIGHT'),
        width: config.get('THUMB_WIDTH')
    }

    console.log('createThumbnailImage', imageSize);

    if (typeof size != 'undefined') {
        imageSize = size;
    }

    var gm = require('gm').subClass({
        imageMagick: true
    });

    gm(originalPath)
        .resize(imageSize.width, imageSize.height)
        .autoOrient()
        .write(thumbnailPath, function(err, data) {
            if (err) {
                return callback(err);
            } else {
                callback(null, thumbnailPath)
            }
        })
}

module.exports = {
    getSignedUrl: getSignedUrl,
    uploadImageFileToS3BucketWithThumbnail: uploadImageFileToS3BucketWithThumbnail,
    uploadFileToS3Bucket: uploadFileToS3Bucket,
};