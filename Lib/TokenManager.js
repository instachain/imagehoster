'use strict';

var Config = require('../constant');
var Jwt = require('jsonwebtoken');
var async = require('async');
var Service = require('../Services');
var Controllers = require('../Controllers');
var config = require('config');
var RequestManager = require('./RequestManager');

function CheckIfControllerNotInitialised() {

    if (!Controllers || !Controllers.makeModule) {
        Controllers = require('../Controllers');
    }
}


var getTokenFromDB = function(userId, userType, loginAs, token, callback) {
    CheckIfControllerNotInitialised();

    var userData = null;
    var criteria = {
        // _id: userId,
        accessToken: token,
        loginAs: loginAs
    };
    async.series([
        function(cb) {

            if (userType == Config.APP_CONSTANTS.DATABASE.userType.User) {

                criteria.userId = userId;

                Controllers.makeModule.accessTokens.validateAccessToken(criteria, function(err, result) {

                    if (err) return cb(err)
                    userData = result;
                    cb();
                })

            } else if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN) {
                Service.makeModule.admins.view(criteria, { loginAttempts: 0 }, { lean: true }, function(err, dataAry) {
                    if (err) {
                        callback(err)
                    } else {
                        if (dataAry && dataAry.length > 0) {
                            userData = dataAry[0];
                            cb();
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });
            } else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function(err, result) {
        if (err) {
            callback(err)
        } else {
            if (userData && userData._id) {
                userData.id = userData._id;
                userData.type = userType;
                userData.role = userType;
            }

            callback(null, { userData: userData })
        }

    });
};




var setTokenInDB = function(tokenData, tokenToSave, callback) {

    CheckIfControllerNotInitialised();
    var userId = tokenData.id;
    var userType = tokenData.type;
    var loginAs = tokenData.loginAs;
    var deviceToken = tokenData.deviceToken;
    var deviceType = tokenData.deviceType;
    var deviceId = tokenData.deviceId;


    var criteria = {
        _id: userId
    };
    var setQuery = {
        accessToken: tokenToSave
    };
    var updatedData = {};
    async.series([

        function(cb) {
            if (userType == Config.APP_CONSTANTS.DATABASE.userType.User) {

                var data = {
                    accessTokenSave: {
                        accessToken: tokenToSave,
                        loginAs: loginAs,
                        deviceToken: deviceToken,
                        deviceType: deviceType,
                        deviceId: deviceId,
                        userId: userId
                    }
                }

                Controllers.makeModule.accessTokens.createNewAccessToken(data, function(err, result) {
                    if (err) return cb(err)

                    updatedData = result.accessTokenData;
                    cb();

                });

            } else if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN) {
                Service.makeModule.admins.edit(criteria, setQuery, { new: true }, function(err, dataAry) {
                    if (err) {
                        cb(err)
                    } else {
                        if (dataAry && dataAry._id) {
                            updatedData = dataAry;
                            cb();
                        } else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }

                });
            } else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function(err, result) {
        if (err) {
            callback(err)
        } else {
            callback(null, updatedData)
        }

    });
};

var expireTokenInDB = function(userId, userType, callback) {
    CheckIfControllerNotInitialised();

    var criteria = {
        _id: userId
    };
    var setQuery = {
        accessToken: new Date().getTime()
    };
    async.series([
        function(cb) {
            if (userType == Config.APP_CONSTANTS.DATABASE.userType.User) {
                Service.makeModule.users.edit(criteria, setQuery, { new: true }, function(err, dataAry) {
                    if (err) {
                        cb(err)
                    } else {
                        if (dataAry && dataAry.length > 0) {
                            cb();
                        } else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                });

            } else if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN) {

                Service.makeModule.admins.edit(criteria, setQuery, { new: true }, function(err, dataAry) {
                    if (err) {
                        console.log(err)

                        callback(err)
                    } else {
                        if (dataAry) {
                            cb(null, 1);
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });
            } else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function(err, result) {
        if (err) {
            callback(err)
        } else {
            callback()
        }

    });
};


var verifyToken = function(token, callback) {
    var response = {
        valid: false
    };
    Jwt.verify(token, config.get('JWT_SECRET_KEY'), function(err, decoded) {
        if (err) {
            console.log('jwt err', err)

            callback(err)
        } else {
            getTokenFromDB(decoded.id, decoded.type, decoded.loginAs, token, callback);
        }
    });
};

var verifyTokenAdmin = function(token, callback) {

    var response = {
        valid: false
    };
    RequestManager.postApi(token, function(err, response) {
        if (err) {
            console.log('*********** RequestManager err *********** ', err)
            callback(err)
        } else {
            return callback(null, response);
        }
    })
};

var verifyTokenManagement = function(token, callback) {
    var response = {
        valid: false
    };
    Jwt.verify(token, config.get('JWT_SECRET_KEY'), function(err, decoded) {
        if (err) {
            console.log('jwt err', err)

            callback(err)
        } else {
            getTokenFromDB(decoded.id, decoded.type, decoded.loginAs, token, function(err, result) {

                if (err)
                    return callback(err);

                return callback(null, result);
            });
        }
    });
};

var setToken = function(tokenData, callback) {


    if (!tokenData.id || !tokenData.type) {
        console.log("err, data==============Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR", Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {

        tokenData.time = new Date().getTime();


        var tokenToSend = Jwt.sign(tokenData, config.get('JWT_SECRET_KEY'));

        setTokenInDB(tokenData, tokenToSend, function(err, data) {
            console.log(err, data)
            callback(err, data)
        })
    }
};

var expireToken = function(token, callback) {
    Jwt.verify(token, config.get('JWT_SECRET_KEY'), function(err, decoded) {
        if (err) {
            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        } else {
            expireTokenInDB(decoded.id, decoded.type, function(err, data) {
                callback(err, data)
            });
        }
    });
};

var decodeToken = function(token, callback) {
    Jwt.verify(token, config.get('JWT_SECRET_KEY'), function(err, decodedData) {
        if (err) {
            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        } else {
            callback(null, decodedData)
        }
    })
};

module.exports = {
    expireToken: expireToken,
    setToken: setToken,
    verifyToken: verifyToken,
    decodeToken: decodeToken,
    verifyTokenAdmin: verifyTokenAdmin,
    verifyTokenManagement: verifyTokenManagement
};