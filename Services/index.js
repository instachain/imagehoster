
var Models = require('../Models');

var serviceModule = require('./ServiceModule').ServiceModule;

var makeModule = {

'notifications' : Models.Notifications

,'emailTemplates' : Models.EmailTemplates

,'apiHistories' : Models.ApiHistories

,'adminConstants' : Models.AdminConstants

,'smsMessages' : Models.SmsMessages

,'accessTokens' : Models.AccessTokens

,'editHistories' : Models.EditHistories

,'admins' : Models.Admins

,'users' : Models.Users

,'images' : Models.Images

,'feedback' : Models.Feedback


// ,'powerPlant' : Models.PowerPlant

,'flagImage' : Models.FlagImage
}

var objectToExport = {

  AppVersionService: require('./AppVersionService'),
  makeModule: {},
};


for (key in makeModule) {
  objectToExport.makeModule[key] = new serviceModule(makeModule[key]);
}


module.exports = objectToExport;
