# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Make sure you have Node.js and the Mongo DB and PM2 installed.

* git clone git@bitbucket.org:instachain/imagehoster.git
* cd imagehoster
* npm install
* npm start


### Summary ###

* For image hosting I have used MongoDB as a database. And it is separate database on it's own, as in microservice we make separate multiple instances of code and database of it's own. so that we can make our individual module scale on it's own according to it's usgae without worrying about the core functionality.

* As I have made the api's for updating the user profile to change into private and public in my own code and it's in the mongodb database. I am saving the image of a user using userId I get from mongodb. As each image is attached with a user id. Consider it like a two tables, users and images and in images table we have userId as a foreign key.


### Structure ###

* Main Folder is "Models" folder it contains individual folder for each group of functionalities. Inside sub folder we have schema, route and controller for that functionalities.

* Other than that we have Lib folder for common functionalities, "constant" for constant keywords, "config" for the configuration.

* we have "golos-js" folder as we can't npm install on it, cause there is bug in the module during npm install, that's why we have to put it inside the code. so that we don't have to make the changes in every new install