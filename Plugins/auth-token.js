'use strict';

var TokenManager = require('../Lib/TokenManager');
var UniversalFunctions = require('../Utils/UniversalFunctions');

const plugin = {

    name: 'auth-token-plugin',

    register: function(server, options) {

        //Register Authorization Plugin

        (async() => {

            await server.register(require('hapi-auth-bearer-token'))

            server.auth.strategy('UserAuth', 'bearer-access-token', {
                allowQueryToken: false,
                allowMultipleHeaders: true,
                accessTokenName: 'accessToken',
                validate: async(request, token, h) => {
                    TokenManager.verifyToken(token, function(err, response) {
                        if (err || !response || !response.userData) {
                            callback(null, false, {
                                token: token,
                                userData: null,

                            })
                        } else {
                            callback(null, true, {
                                token: token,
                                userData: response.userData
                            })
                        }
                    });

                }
            });

            server.auth.strategy('AdminAuth', 'bearer-access-token', {
                allowQueryToken: false,
                allowMultipleHeaders: true,
                accessTokenName: 'accessToken',
                validate: async(request, token, h) => {

                    return new Promise((resolve) => {

                        TokenManager.verifyTokenAdmin(token, function(err, response) {

                            if (err || !response) {
                              
                                const isValid = false;
                                const credentials = {
                                    token: token,
                                    adminData: null
                                };
                                resolve({ isValid, credentials, credentials });

                            } else {

                                const isValid = true;
                                const credentials = {
                                    token: token,
                                    adminData: response
                                };
                                resolve({ isValid, credentials, credentials });
                            }
                        });
                    });
                }
            });

            server.auth.strategy('OrgAdminAuth', 'bearer-access-token', {
                allowQueryToken: false,
                allowMultipleHeaders: true,
                accessTokenName: 'accessToken',
                validate: async(request, token, h) => {
                    TokenManager.verifyTokenManagement(token, function(err, response) {

                        if (err || !response || !response.userData) {
                            callback(null, false, {
                                token: token,
                                userData: null
                            })
                        } else {

                            callback(null, true, {
                                token: token,
                                userData: response.userData
                            })
                        }
                    });
                }
            });
        })();
    }
}

module.exports = plugin